<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>echarts demo</title>
</head>
<body>
	<div id="bar" style="width:600px;height:400px;"></div>
	<div id="pie" style="width:600px;height:400px;"></div>
	<div id="line" style="width:800px;height:400px;"></div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts.js"></script>
<script type="text/javascript">
	$(function() {
		echarts.init(document.getElementById('bar')).setOption(optionBar);
		echarts.init(document.getElementById('pie')).setOption(optionPie);
		echarts.init(document.getElementById('line')).setOption(optionLine);
	});

	var optionBar = {
		title : {
			text : '柱状图统计'
		},
		tooltip : {},
		legend : {
			data : [ '销量', '价格', '利润', '增长率' ]
		},
		xAxis : {
			name : 'xAxis',
			data : [ '衬衫', '羊毛衫', '雪纺衫', '裤子', '高跟鞋', '袜子' ]
		},
		yAxis : {
			name : 'yAxis'
		},
		series : [ {
			name : '销量',
			type : 'bar',
			data : [ 5, 20, 36, 10, 10, 20 ]
		}, {
			name : '价格',
			type : 'bar',
			data : [ 42, 50, 26, 13, 30, 66 ],
			//箭头最低指向最高
			markLine : {
				lineStyle : {
					normal : {
						type : 'dashed'
					}
				},
				data : [ [ {
					// 起点和终点的项会共用一个 name
					name : '最小值到最大值',
					type : 'min'
				}, {
					type : 'max'
				} ] ]
			}
		}, {
			name : '利润',
			type : 'bar',
			data : [ 12, 20, 36, 13, 50, 16 ]
		}, {
			name : '增长率',
			type : 'bar',
			data : [ 12, 20, 36, 13, 50, 16 ]
		} ]
	};

	var optionPie = {
		title : {
			text : '某站点用户访问来源',
			subtext : '纯属虚构',
			x : 'center'
		},
		tooltip : {
			trigger : 'item',
			formatter : "{a} <br/>{b} : {c} ({d}%)"
		},
		legend : {
			orient : 'vertical',
			left : 'left',
			data : [ '直接访问', '邮件营销', '联盟广告', '视频广告', '搜索引擎' ]
		},
		series : [ {
			name : '访问来源',
			type : 'pie',
			radius : '55%',
			center : [ '50%', '60%' ],
			data : [ {
				value : 335,
				name : '直接访问'
			}, {
				value : 310,
				name : '邮件营销'
			}, {
				value : 234,
				name : '联盟广告'
			}, {
				value : 135,
				name : '视频广告'
			}, {
				value : 1548,
				name : '搜索引擎'
			} ],
			itemStyle : {
				emphasis : {
					shadowBlur : 10,
					shadowOffsetX : 0,
					shadowColor : 'rgba(0, 0, 0, 0.5)'
				}
			}
		} ]
	};

	var optionLine = {
		title : {
			text : '堆叠区域图'
		},
		tooltip : {
			trigger : 'axis'
		},
		legend : {
			data : [ '邮件营销', '联盟广告', '视频广告', '直接访问', '搜索引擎' ]
		},
		toolbox : {
			feature : {
				saveAsImage : {}
			}
		},
		grid : {
			left : '3%',
			right : '4%',
			bottom : '3%',
			containLabel : true
		},
		xAxis : [ {
			type : 'category',
			boundaryGap : false,
			data : [ '周一', '周二', '周三', '周四', '周五', '周六', '周日' ]
		} ],
		yAxis : [ {
			type : 'value'
		} ],
		series : [ {
			name : '邮件营销',
			type : 'line',
			stack : '总量',
			areaStyle : {
				normal : {}
			},
			data : [ 120, 132, 101, 134, 90, 230, 210 ]
		}, {
			name : '联盟广告',
			type : 'line',
			stack : '总量',
			areaStyle : {
				normal : {}
			},
			data : [ 220, 182, 191, 234, 290, 330, 310 ]
		}, {
			name : '视频广告',
			type : 'line',
			stack : '总量',
			areaStyle : {
				normal : {}
			},
			data : [ 150, 232, 201, 154, 190, 330, 410 ]
		}, {
			name : '直接访问',
			type : 'line',
			stack : '总量',
			areaStyle : {
				normal : {}
			},
			data : [ 320, 332, 301, 334, 390, 330, 320 ]
		}, {
			name : '搜索引擎',
			type : 'line',
			stack : '总量',
			label : {
				normal : {
					show : true,
					position : 'top'
				}
			},
			areaStyle : {
				normal : {}
			},
			data : [ 820, 932, 901, 934, 1290, 1330, 1320 ]
		} ]
	};
</script>
</html>