/**
 * jquery.validate.js 配置
 */
jQuery.extend(jQuery.validator.messages, {
	required : "该项为必填项",
	email : "请输入正确的电子邮件地址",
	url : "请输入正确的网址",
	date : "请输入合法的日期",
	number : "请输入合法的数字",
	digits : "请输入整数",
	creditcard : "请输入合法的信用卡号",
	equalTo : "两次输入不一致",
	accept : "请输入拥有合法后缀名的字符串",
	rangelength : jQuery.validator.format("请输入 {0} 到 {1} 个字符"),
	range : jQuery.validator.format("请输入一个大于 {0} 小于 {1} 的值"),
	max : jQuery.validator.format("请输入一个最大为 {0} 的值"),
	min : jQuery.validator.format("请输入一个最小为 {0} 的值")
});
// 固定长度
jQuery.validator.addMethod("fixlength", function(value, element, param) {
	var length = value.length;
	return this.optional(element) || (length == param);
}, $.validator.format("请输入 {0} 个字符"));
// 限制最大长度
jQuery.validator.addMethod("maxlen", function(value, element, param) {
	var length = value.length;
	return this.optional(element) || (length <= param);
}, $.validator.format("请输入  {0} 个以内字符"));
//限制最小长度
jQuery.validator.addMethod("minlen", function(value, element, param) {
	var length = value.length;
	return this.optional(element) || (length >= param);
}, $.validator.format("请至少输入  {0} 个字符"));
// 身份证号
jQuery.validator.addMethod("idcard", function(value, element, param) {
	var reg = /^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$|^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}([0-9]|X|x)$/;
	return this.optional(element) || reg.test(value);
}, $.validator.format("请输入正确的身份证号码"));
// 手机号
jQuery.validator.addMethod("mobile", function(value, element, param) {
	var reg = /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
	return this.optional(element) || reg.test(value);
}, $.validator.format("请输入正确的手机号码"));
// 中文字两个字节范围长度
jQuery.validator.addMethod("rangelenbyte", function(value, element, param) {
	var length = value.length;
	for ( var i = 0; i < value.length; i++) {
		if (value.charCodeAt(i) > 127) {
			length++;
		}
	}
	return this.optional(element) || (length >= param[0] && length <= param[1]);
}, $.validator.format("请输入 {0} 到 {1} 个字符(一个中文字算2个字符)"));
//密码验证
jQuery.validator.addMethod("checkPwd", function(value, element, param) {
	var reg = /^(?!^\d+$)(?!^[a-zA-Z]+$)[\da-zA-Z]{6,15}$/;
	return this.optional(element) || reg.test(value);
}, $.validator.format("必须包含数字及字母，长度为6-15位！"));
//域名验证(允许url、ip和domain)
jQuery.validator.addMethod("domain", function(value, element, param) {
	var regIP = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
	var regDomain = /^([0-9a-z_!~*'()-]+\.)*([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.[a-z]{2,6}$/i;
	var regUrl = /^((https|http|ftp|rtsp|mms)?:\/\/)(\S+\.)+\S{2,}$/i;
	return this.optional(element) || regUrl.test(value) || regDomain.test(value) || (regIP.test(value) && (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256 && RegExp.$4 < 256));
}, $.validator.format("请输入正确链接地址或域名格式！"));
//mac地址验证
jQuery.validator.addMethod("checkMac", function(value, element, param) {
	var reg = /^[a-fA-F\d]{2}:[a-fA-F\d]{2}:[a-fA-F\d]{2}:[a-fA-F\d]{2}:[a-fA-F\d]{2}:[a-fA-F\d]{2}$/;
	return this.optional(element) || reg.test(value);
}, $.validator.format("mac地址格式不正确！"));



//判断整数value是否等于0 
jQuery.validator.addMethod("isIntEqZero", function(value, element) { 
     value=parseInt(value);      
     return this.optional(element) || value==0;       
}, "整数必须为0"); 
  
// 判断整数value是否大于0
jQuery.validator.addMethod("isIntGtZero", function(value, element) { 
     value=parseInt(value);      
     return this.optional(element) || value>0;       
}, "整数必须大于0"); 
  
// 判断整数value是否大于或等于0
jQuery.validator.addMethod("isIntGteZero", function(value, element) { 
     value=parseInt(value);      
     return this.optional(element) || value>=0;       
}, "整数必须大于或等于0");   

// 判断整数value是否不等于0 
jQuery.validator.addMethod("isIntNEqZero", function(value, element) { 
     value=parseInt(value);      
     return this.optional(element) || value!=0;       
}, "整数必须不等于0");  

// 判断整数value是否小于0 
jQuery.validator.addMethod("isIntLtZero", function(value, element) { 
     value=parseInt(value);      
     return this.optional(element) || value<0;       
}, "整数必须小于0");  

// 判断整数value是否小于或等于0 
jQuery.validator.addMethod("isIntLteZero", function(value, element) { 
     value=parseInt(value);      
     return this.optional(element) || value<=0;       
}, "整数必须小于或等于0");  

// 判断浮点数value是否等于0 
jQuery.validator.addMethod("isFloatEqZero", function(value, element) { 
     value=parseFloat(value);      
     return this.optional(element) || value==0;       
}, "浮点数必须为0"); 
  
// 判断浮点数value是否大于0
jQuery.validator.addMethod("isFloatGtZero", function(value, element) { 
     value=parseFloat(value);      
     return this.optional(element) || value>0;       
}, "浮点数必须大于0"); 
  
// 判断浮点数value是否大于或等于0
jQuery.validator.addMethod("isFloatGteZero", function(value, element) { 
     value=parseFloat(value);      
     return this.optional(element) || value>=0;       
}, "浮点数必须大于或等于0");   

// 判断浮点数value是否不等于0 
jQuery.validator.addMethod("isFloatNEqZero", function(value, element) { 
     value=parseFloat(value);      
     return this.optional(element) || value!=0;       
}, "浮点数必须不等于0");  

// 判断浮点数value是否小于0 
jQuery.validator.addMethod("isFloatLtZero", function(value, element) { 
     value=parseFloat(value);      
     return this.optional(element) || value<0;       
}, "浮点数必须小于0");  

// 判断浮点数value是否小于或等于0 
jQuery.validator.addMethod("isFloatLteZero", function(value, element) { 
     value=parseFloat(value);      
     return this.optional(element) || value<=0;       
}, "浮点数必须小于或等于0");  

// 判断浮点型  
jQuery.validator.addMethod("isFloat", function(value, element) {       
     return this.optional(element) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
}, "只能包含数字、小数点等字符"); 
 
// 匹配integer
jQuery.validator.addMethod("isInteger", function(value, element) {       
     return this.optional(element) || (/^[-\+]?\d+$/.test(value) && parseInt(value)>=0);       
}, "匹配integer");  
 
// 判断数值类型，包括整数和浮点数
jQuery.validator.addMethod("isNumber", function(value, element) {       
     return this.optional(element) || /^[-\+]?\d+$/.test(value) || /^[-\+]?\d+(\.\d+)?$/.test(value);       
}, "匹配数值类型，包括整数和浮点数");  

// 只能输入[0-9]数字
jQuery.validator.addMethod("isDigits", function(value, element) {       
     return this.optional(element) || /^\d+$/.test(value);       
}, "只能输入0-9数字");  

// 判断中文字符 
jQuery.validator.addMethod("isChinese", function(value, element) {       
     return this.optional(element) || /^[\u0391-\uFFE5]+$/.test(value);       
}, "只能包含中文字符。");   

// 判断英文字符 
jQuery.validator.addMethod("isEnglish", function(value, element) {       
     return this.optional(element) || /^[A-Za-z]+$/.test(value);       
}, "只能包含英文字符。");   

 // 手机号码验证    
jQuery.validator.addMethod("isMobile", function(value, element) {    
  var length = value.length;    
  return this.optional(element) || (length == 11 && /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(value));    
}, "请正确填写您的手机号码。");

// 电话号码验证    
jQuery.validator.addMethod("isPhone", function(value, element) {    
  var tel = /^(\d{3,4}-?)?\d{7,9}$/g;    
  return this.optional(element) || (tel.test(value));    
}, "请正确填写您的电话号码。");

// 联系电话(手机/电话皆可)验证   
jQuery.validator.addMethod("isTel", function(value,element) {   
    var length = value.length;   
    var mobile = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/;   
    var tel = /^(\d{3,4}-?)?\d{7,9}$/g;       
    return this.optional(element) || tel.test(value) || (length==11 && mobile.test(value));   
}, "请正确填写您的联系方式"); 

 // 匹配qq      
jQuery.validator.addMethod("isQq", function(value, element) {       
     return this.optional(element) || /^[1-9]\d{4,12}$/;       
}, "匹配QQ");   

 // 邮政编码验证    
jQuery.validator.addMethod("isZipCode", function(value, element) {    
  var zip = /^[0-9]{6}$/;    
  return this.optional(element) || (zip.test(value));    
}, "请正确填写您的邮政编码。");  

// 匹配密码，以字母开头，长度在6-12之间，只能包含字符、数字和下划线。      
jQuery.validator.addMethod("isPwd", function(value, element) {       
     return this.optional(element) || /^[a-zA-Z]\\w{6,12}$/.test(value);       
}, "以字母开头，长度在6-12之间，只能包含字符、数字和下划线。");  

// 身份证号码验证
jQuery.validator.addMethod("isIdCardNo", function(value, element) { 
  //var idCard = /^(\d{6})()?(\d{4})(\d{2})(\d{2})(\d{3})(\w)$/;   
  return this.optional(element) || isIdCardNo(value);    
}, "请输入正确的身份证号码。"); 

// IP地址验证   
jQuery.validator.addMethod("ip", function(value, element) {    
  return this.optional(element) || /^(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.)(([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))\.){2}([1-9]|([1-9]\d)|(1\d\d)|(2([0-4]\d|5[0-5])))$/.test(value);    
}, "请填写正确的IP地址。");

// 字符验证，只能包含中文、英文、数字、下划线等字符。    
jQuery.validator.addMethod("stringCheck", function(value, element) {       
     return this.optional(element) || /^[a-zA-Z0-9\u4e00-\u9fa5-_]+$/.test(value);       
}, "只能包含中文、英文、数字、下划线等字符");   

// 匹配english  
jQuery.validator.addMethod("isEnglish", function(value, element) {       
     return this.optional(element) || /^[A-Za-z]+$/.test(value);       
}, "匹配english");   

// 匹配汉字  
jQuery.validator.addMethod("isChinese", function(value, element) {       
     return this.optional(element) || /^[\u4e00-\u9fa5]+$/.test(value);       
}, "匹配汉字");   

// 匹配中文(包括汉字和字符) 
jQuery.validator.addMethod("isChineseChar", function(value, element) {       
     return this.optional(element) || /^[\u0391-\uFFE5]+$/.test(value);       
}, "匹配中文(包括汉字和字符) "); 
  
// 判断是否为合法字符(a-zA-Z0-9-_)
jQuery.validator.addMethod("isRightfulString", function(value, element) {       
     return this.optional(element) || /^[A-Za-z0-9_-]+$/.test(value);       
}, "判断是否为合法字符(a-zA-Z0-9-_)");   

// 判断是否包含中英文特殊字符，除英文"-_"字符外
jQuery.validator.addMethod("isContainsSpecialChar", function(value, element) {  
     var reg = RegExp(/[(\ )(\`)(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\+)(\=)(\|)(\{)(\})(\')(\:)(\;)(\')(',)(\[)(\])(\.)(\<)(\>)(\/)(\?)(\~)(\！)(\@)(\#)(\￥)(\%)(\…)(\&)(\*)(\（)(\）)(\—)(\+)(\|)(\{)(\})(\【)(\】)(\‘)(\；)(\：)(\”)(\“)(\’)(\。)(\，)(\、)(\？)]+/);   
     return this.optional(element) || !reg.test(value);       
}, "含有中英文特殊字符");   