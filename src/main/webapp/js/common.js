/**========================================*/
/**
 * 全局变量配置<br>
 */
/** js工具类定义对象 */
var U = {};
/** 项目路径 eg：/admin-hui */
var app_path = document.location.pathname.substr(0, document.location.pathname.substr(1).indexOf("/") + 1);
/** ztree图标地址 */
var ztreeIconPath = app_path + "/static/H-ui.admin_v3.0/lib/zTree/v3/css/zTreeStyle/img/diy/";
/** js版本，防止旧版本缓存js */
var version = new Date().getTime();
var msg_offline = "网络异常，请检查网络是否通畅";
var msg_noSession = "会话已超时，请重新登录";
var msg_notFound = "加载网页失败";
/** 默认头像 */
U.defalutHead = app_path + "/images/head.png";
/** 图片style置灰色（不支持ie9+） */
U.grey = "-webkit-filter: grayscale(100%);-moz-filter: grayscale(100%);-ms-filter: grayscale(100%);-o-filter: grayscale(100%);filter: grayscale(100%);filter: gray";
/** 消息提醒id（防止重复弹层） */
var indexId = "";// "notice_index";
/** 引入常用JS和CSS，加参数的表示不需要缓存的js */
document.write("<script language='javascript' src='" + app_path + "/js/config.js'></script>");
document.write("<script language='javascript' src='" + app_path + "/js/jquery.js'></script>");
document.write("<script language='javascript' src='" + app_path + "/js/core.js?" + version + "'></script>");
document.write("<script language='javascript' src='" + app_path + "/js/jquery.timer.js'></script>");
document.write("<script language='javascript' src='" + app_path + "/static/layer/layer.js'></script>");
document.write("<script language='javascript' src='" + app_path + "/static/laypage/laypage.js'></script>");
/**
 * =========================================
 * 
 * 注意:#######common.js为网页前端组件，只针对于bootstrap或者类似风格的网页开发，不适用于基于extjs、easyui等组件#######<br>
 * 
 * 手动配置参数<br>
 * 引入其他js组件路径 <br>
 * 顶部全局变量配置 <br>
 * U.loadPage的pageNo、_index、_resp等属性 <br>
 * U.loadHtml的跳转登录页面、判断登录页面 <br>
 * checkExceptionAndPermission <br>
 * U.showHtmlDialog的404.html <br>
 * U.loadPCA的city.json地址 <br>
 * 
 * ==========================================
 */
/**
 * Ajax请求封装
 * @param url 请求连接，从项目路径后开始
 * @param data 参数
 * @param callback 回调函数
 * @param async 是否异步，默认true为异步
 * @param isShowNotice 是否显示弹框，用来处理notice警告时交给自动处理（弹框）还是手动处理
 * @auther chensi
 * @version 2015-5-26 上午11:23:37
 */
function ajaxCall(url, data, callback, async, isShowNotice) {
	$.ajax({
		async : async == false ? false : true,
		url : app_path + '/' + url,
		data : data,
		type : "POST",
		dataType : "json",
		success : function(_resp) {
			if (C.isEmpty(_resp)) {
				U.showExceptionDialog();
				U.closeAll("loading");
				return;
			}
			if (U.checkExceptionAndPermission(_resp, isShowNotice)) {
				if (callback) {
					callback(_resp);
				}
			} else {
				U.closeAll("loading");
			}
		},
		error : function(_xhr, _errormsg, _e) {
			if (_xhr.status === 0 || _xhr.status === 503) {
				console.log(_xhr.status);
				U.alert(msg_offline);
			} else if (_xhr.status === 401) {
				U.confirm(msg_noSession, function() {
					window.location = app_path + '/login.html';
				});
			} else {
				U.showExceptionDialog();
			}
			U.closeAll("loading");
		}
	});
}

/**
 * 异步提交表单
 * @param $dom 表单
 * @param url 提交地址
 * @param callback 回调函数
 * @param isShowNotice 是否显示弹框，用来处理notice警告时交给自动处理（弹框）还是手动处理
 * @auther chensi
 * @version 2015-6-25 下午11:22:45
 */
function ajaxSubmit($dom, url, callback, isShowNotice) {
	$dom.ajaxSubmit({
		type : "post",
		url : app_path + '/' + url,
		success : function(_resp) {
			if (C.isEmpty(_resp)) {
				U.showExceptionDialog();
				U.closeAll("loading");
				return;
			}
			if (U.checkExceptionAndPermission(_resp, isShowNotice)) {
				if (callback) {
					callback(_resp);
				}
			} else {
				U.closeAll("loading");
			}
		},
		error : function(xhr, _errormsg, _e) {
			if (xhr.status === 0 || xhr.status === 503) {
				U.alert(msg_offline);
			} else if (_xhr.status === 401) {
				U.confirm(msg_noSession, function() {
					window.location = app_path + '/login.html';
				});
			} else {
				U.showExceptionDialog();
			}
			U.closeAll("loading");
		}
	});
}

/**
 * 根据请求地址获取HTML信息
 * 
 * @param url
 * @param data
 * @param callback
 * @param async
 * @auther chensi
 */
U.loadHtml = function(url, data, callback, async) {
	$.ajax({
		async : async == false ? false : true,
		url : app_path + '/' + url,
		data : data,
		type : "POST",
		dataType : "text",
		success : function(data, flag, xhr) {
			var response = xhr.responseText;
			// 如果load的页面是登录页面，直接返回到登录页面
			if (xhr.responseText.indexOf('<html pageIdentity="login">') != -1) {
				window.location = app_path + '/login.html';
				return;
			}
			if (callback) {
				callback(data);
			}
		},
		error : function(xhr, _errormsg, _e) {
			if (xhr.status === 0 || xhr.status === 503) {
				U.alert(msg_offline);
			} else if (_xhr.status === 401) {
				U.confirm(msg_noSession, function() {
					window.location = app_path + '/login.html';
				});
			} else if (xhr.status == 404) {
				U.alert(msg_notFound);
			} else {
				U.showExceptionDialog();
			}
			U.closeAll("loading");
		}
	});
}

/**
 * ajax分页，基于layerPage(耦合了java Pager类)
 * @param $dataItemTemp 模板dom
 * @param $dataList 列表dom
 * @param $page 分页组件dom
 * @param url 请求地址（eg："user/page.json"）
 * @param param 参数（json）--（缺省调默认值）
 * @param filter 过滤函数 自定义模板变量
 * @param pageNo 页码--（缺省调默认值）
 * @auther chensi
 */
U.loadPage = function($dataItemTemp, $dataList, $page, url, param, filter, pageNo) {
	var loadingIndex = U.loading();
	pageNo = pageNo || 1;
	ajaxCall(url + "?pageNo=" + pageNo, param, function(_resp) {
		var dataHtml = "";
		$.each(_resp.data.list, function(i, item) {
			if (filter) {
				filter(item);
			}
			// 索引字段为{{_index}}
			item._index = i + 1 + (pageNo - 1) * _resp.data.pageSize;
			dataHtml += C.template($dataItemTemp.html(), item);
		});
		$dataList.empty();// 清除列表
		$page.empty();// 清除分页
		if (C.isEmpty(dataHtml) || dataHtml.length == 0) {
			$dataList.append("<tr><td colspan='99' style='text-align: center;'>暂无数据</td></tr>");
			layer.close(loadingIndex);
			return;
		} else {
			$dataList.append(dataHtml);
		}
		laypage({
			cont : $page, // 容器。值支持id名、原生dom对象，jquery对象。
			pages : _resp.data.totalPage, // 通过后台拿到的总页数
			curr : pageNo, // 当前页
			skip : true, // 是否开启跳页
			skin : '#63B8FF',
			groups : 8, // 连续显示分页数，默认是5
			jump : function(obj, first) { // 触发分页后的回调
				if (!first) { // 点击跳页触发函数自身，并传递当前页：obj.curr
					U.loadPage($dataItemTemp, $dataList, $page, url, param, filter, obj.curr);
				}
			}
		});
		layer.close(loadingIndex);
	});
}

/**
 * 检查请求是否出现异常或权限问题
 * 
 * @param _resp 返回数据
 * @param isShowNotice 是否直接弹出提示(设置为false时, 不弹出提示)
 * @auther chensi
 * @version 2015-6-25 下午11:22:45
 */
U.checkExceptionAndPermission = function(_resp, isShowNotice) {
	if (_resp.status == '000001') { // 请求出错
		U.alert(_resp.message);
		return false;
	}
	if (_resp.status == '000002') { // 警告提示，并允许回调
		// 是否弹出提示框
		if (isShowNotice) {
			U.alert(_resp.message);
		}
		return true;
	}
	if (_resp.status == '000003') { // 无权限
		// U.confirm("您暂无权限,请重新登录", function() {
		// window.location = app_path + '/login.html';
		// });
		return false;
	}
	if (_resp.status == '000004') { // 重定向
		window.location = app_path + _resp.data;
		return false;
	}
	return true;
}

/**
 * 显示出错提示
 * 
 * @auther chensi
 * @version 2015-6-26 上午8:42:19
 */
U.showExceptionDialog = function(_resp) {
	var errMsg = "系统繁忙，请稍后再试";
	if (!C.isEmpty(_resp) && !C.isEmpty(_resp.message)) {
		errMsg += "<br>错误信息: " + _resp.message;
	}
	U.alert(errMsg);
}

/**
 * 加载层
 * 
 * @auther chensi
 * @version 2015-6-1 下午4:48:17
 */
U.loading = function() {
	return layer.msg('加载中', {
		  icon: 16
		  ,shade: 0.1
		});
	/*return layer.load(2, {
		shade : [ 0.1, '#fff' ]
	});*/
}


// layer.closeAll(); //疯狂模式，关闭所有层
// layer.closeAll('dialog'); //关闭信息框
// layer.closeAll('page'); //关闭所有页面层
// layer.closeAll('iframe'); //关闭所有的iframe层
// layer.closeAll('loading'); //关闭加载层
// layer.closeAll('tips'); //关闭所有的tips层
/**
 * 关闭所有层
 */
U.closeAll = function(type) {
	layer.closeAll(type);
}

/**
 * 半透明黑色提示，过几秒消失
 * @auther chensi
 */
U.msg = function(message, time, callback) {
	layer.msg((message || "加载中..."), {
		time : time || 2000
	}, function() {
		if (callback) {
			callback();
		}
	});
}

/**
 * 消息通知（自动消失）
 */
U.notice = function(html, id) {
	id = id || indexId;
	layer.open({
		id : id,// 设置该值后，不管是什么类型的层，都只允许同时弹出一个。一般用于页面层和iframe层模式
		type : 1, // page层
		area : [ '260px', '150px' ],
		title : '消息通知',
		shade : 0, // 不遮罩
		offset : 'rb',// 右下角
		moveType : 1, // 拖拽风格，0是默认，1是传统拖动
		shift : 2, // 0-6的动画形式，-1不开启
		time : 5000,// 消失时间
		skin : 'layui-layer-molv',// 皮肤
		move : false,// 禁止拖拽
		zIndex : 20000000,// 最上层(默认19891014)
		content : "<div style='padding:20px;'>" + html + "</div>"
	});
}

/**
 * 提示框
 * @param 需提示的文本
 * @auther chensi
 */
U.alert = function(message, callback) {
	var alertIndex = layer.alert(message, {
		title : "提示"
	}, function() {
		layer.close(alertIndex);
		if (callback) {
			callback();
		}
	});
}

/**
 * 超时弹框
 */
U.overtime = function(message, callback) {
	var alertIndex = layer.alert(message, {
		title : "提示",
		closeBtn : 0
	}, function() {
		layer.close(alertIndex);
		if (callback) {
			callback();
		}
	});
}

/**
 * 显示确认提示框
 * 
 * @param message 提示消息
 * @param callback 点击确认后回调
 * @auther chensi
 * @version 2015-6-1 下午4:48:06
 */
U.confirm = function(message, callback) {
	var confirm_index = layer.confirm(message, {
		btn : [ '确认', '取消' ],
		shadeClose : false
	}, function() {
		layer.close(confirm_index);
		if (callback) {
			callback();
		}
	}, function() {
		layer.close(confirm_index);
	});
};

/**
 * 显示选择提示框
 * 
 * @param message 提示消息
 * @param btn1 第一个按钮文本
 * @param btn2 第二个按钮文本
 * @param callback1 第一个按钮点击后的回调
 * @param callback2 第二个按钮点击后的回调
 * @auther chensi
 */
U.choose = function(message, btn1, btn2, callback1, callback2) {
	var confirm_index = layer.confirm(message, {
		btn : [ btn1, btn2 ],
		shadeClose : false
	}, function() {
		layer.close(confirm_index);
		if (callback1) {
			callback1();
		}
	}, function() {
		layer.close(confirm_index);
		if (callback2) {
			callback2();
		}
	});
}

/**
 * 弹出窗口封装(内容为div)
 * @auther chensi
 */
U.showDialog = function(options) {
	var index;
	options = options || {};
	options.title = options.title || '信息框';
	options.width = options.width || '630px';
	options.type = options.type || 1;
	if (options.height) {
		options.area = [ options.width, options.height ];
	} else {
		options.area = options.width;
	}
	U.loadHtml(options.url, options.params, function(data) {
		index = layer.open({
			type : options.type,
			title : options.title,
			area : options.area,
			shade : 0.5,
			content : data,
			btn : null,
			success : options.success,
			end : options.end
		});
	}, false);
	return index;
}

/**
 * 弹出窗口封装（内容为url连接）
 * @param title 标题
 * @param url 请求的url
 * @param w 弹出层宽度，无单位
 * @param h 弹出层高度，无单位
 * @auther chensi
 */
U.showHtmlDialog = function(title, url, w, h, isFull) {
	isFull = isFull || false;
	if (title == null || title == '') {
		title = false;
	}
	if (url == null || url == '') {
		url = "404.html";
	}
	if (w == null || w == '') {
		w = 800;
	}
	if (h == null || h == '') {
		h = ($(window).height() - 50);
	}
	var index = layer.open({
		type : 2,
		area : [ w + 'px', h + 'px' ],
		fix : false, // 不固定
		maxmin : true,
		shade : 0.4,
		title : title,
		content : url,
	});
	if (isFull) {
		layer.full(index);
	}
}

/**
 * 创建并初始化下拉框
 * 
 * @param options 配置信息
 * @auther chensi
 */
U.initSelect = function(options) {
	options.idField = options.idField || "id";
	options.nameField = options.nameField || "name";
	options.appendData = options.appendData === false ? false : true;
	options.data = options.data || {};
	ajaxCall(options.url, options.data, function(_resp) {
		var data = _resp.data;
		var html = "<option value=''>请选择</option>";
		if (options.appendData) {
			$.each(data, function(key, val) {
				var id = eval("val." + options.idField);
				var name = eval("val." + options.nameField);
				html += "<option value='" + id + "'>" + name + "</option>";
			});
			options.$dom.html(html);
		}
		if (options.callback) {
			options.callback(data);
		}
	});
}

/**
 * 省市区三级联动
 * @param $province 省的dom
 * @param $city 城市dom
 * @param $area 区县dom
 * @param provinceDefault 省份回显值
 * @param cityDefault 城市回显值
 * @param areaDefault 区县回显值
 * @auther chensi
 */
U.loadPCA = function($province, $city, $area, provinceDefault, cityDefault, areaDefault) {
	var cityJson;
	$.ajaxSettings.async = false;
	$.getJSON(app_path + "/js/city.json", function(data) {
		cityJson = data;
	});
	$province.on("change", function() {
		U.loadCity(cityJson, $province, $city, cityDefault || '');
	});
	$city.on("change", function() {
		U.loadArea(cityJson, $province, $city, $area, areaDefault || '');
	});
	// 首次触发change必须放在事件函数下面，否则触发了change事件时，事件函数还未加载，导致触发无效。
	U.loadProvince(cityJson, $province, provinceDefault || '');
}
/**
 * 加载省份（U.loadProvince U.loadCity U.loadArea结合使用）
 * @auther chensi
 */
U.loadProvince = function(cityJson, $province, provinceDefault) {
	var temp_html = "<option value=''>请选择</option>";
	$.each(cityJson, function(i, province) {
		temp_html += "<option value='" + province.name + "'>" + province.name + "</option>";
	});
	$province.html(temp_html);
	if (provinceDefault != '') {
		$province.val(provinceDefault);
	}
	$province.trigger("change");
}

/**
 * 加载城市（U.loadProvince U.loadCity U.loadArea结合使用）
 * @auther chensi
 */
U.loadCity = function(cityJson, $province, $city, cityDefault) {
	var province = $province.val();
	if (province == "") {
		$city.html("<option value=''>请选择</option>");
		$area.html("<option value=''>请选择</option>");
		return;
	}
	var index = $province.find(":selected").index() - 1;
	var temp_html = "";
	$.each(cityJson[index].city, function(i, city) {
		temp_html += "<option value='" + city.name + "'>" + city.name + "</option>";
	});
	$city.html(temp_html);
	if (cityDefault != '') {
		$city.val(cityDefault);
	}
	$city.trigger("change");
}

/**
 * 加载区县（U.loadProvince U.loadCity U.loadArea结合使用）
 * @auther chensi
 */
U.loadArea = function(cityJson, $province, $city, $area, areaDefault) {
	var city = $city.val();
	if (city == "") {
		$area.html("<option value=''>请选择</option>");
		return;
	}
	var indexProvince = $province.find(":selected").index() - 1;// 由于第一项为“请选择”，province的坐标从1开始
	var indexCity = $city.find(":selected").index();
	var temp_html = "";
	$.each(cityJson[indexProvince].city[indexCity].area, function(i, area) {
		temp_html += "<option value='" + area + "'>" + area + "</option>";
	});
	$area.html(temp_html);
	if (areaDefault != '') {
		$area.val(areaDefault);
	}
}

/**
 * 获取ztree所有checked的节点，返回id数组
 */
U.getzTreeAllChecked = function(ztree) {
	var arr = [];
	var nodes = ztree.transformToArray(ztree.getNodes());
	for ( var i = 0; i < nodes.length; i++) {
		if (nodes[i].checked == true) {
			arr.push(nodes[i].id);
		}
	}
	return arr;
}

/**
 * Cookie操作 - 设置值
 * @auther chensi
 */
U.setCookie = function(name, value, expire) {
	var exp = new Date();
	exp.setTime(exp.getTime() + expire * 24 * 60 * 60 * 1000);
	document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
};

/**
 * Cookie操作 - 获取值
 * @auther chensi
 */
U.getCookie = function(key) {
	var cookies = document.cookie ? document.cookie.split('; ') : [];
	for ( var i = 0, l = cookies.length; i < l; i++) {
		var parts = cookies[i].split('=');
		var name = parts.shift();
		var cookie = parts.join('=');
		if (key && key === name) {
			return cookie;
		}
	}
};
