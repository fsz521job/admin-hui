<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>授权</title>
</head>
<body id="role_grant">
	<div class="page-container">
		<div class="cl pd-5 bg-1 bk-gray mt-20">
			<span class="l"> <a href="javascript:;" id="save" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe632;</i> 保存</a> <a href="javascript:;"
				id="open" class="btn btn-success radius"><i class="Hui-iconfont">&#xe6de;</i> 展开</a> <a href="javascript:;" id="close" class="btn btn-secondary radius"><i
					class="Hui-iconfont">&#xe6dc;</i> 收起</a> </span>
		</div>
		<div class="text-c mt-20">
			<ul id="tree" class="ztree"></ul>
		</div>

	</div>
</body>
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	var $dom = $("#role_grant");
	var $tree = $dom.find("#tree");

	var tree;
	$(function() {
		/**加载树*/
		loadTree()
		
		/**展开*/
		$dom.find("#open").click(function(){
			tree.expandAll(true); 
		});
		
		/**收起*/
		$dom.find("#close").click(function(){
			tree.expandAll(false); 
		});
		
		/**保存权限*/
		$dom.find("#save").click(function(){
			var ids=U.getzTreeAllChecked(tree)+"";
			ajaxCall("role/grant.do",{"id":${id},"menuIds":ids},function(_rsp){
				U.msg('操作成功', 500, function() {
					var index = window.parent.layer.getFrameIndex(window.name);
					window.parent.layer.close(index);
				});
			});
		});
		
	});
	
	/**加载树*/
	function loadTree(){
		ajaxCall("role/listMyTree.do", {"id": ${id}}, function(_rsp) {
			tree = $.fn.zTree.init($tree, setting, _rsp.data);
			tree.expandAll(true);
		});
	}
	
	/**ztree配置*/
	var setting = {
		check : {
			enable : true
		},
		data : {
			simpleData : {
				enable : true
			}
		}
	};
</script>
</html>