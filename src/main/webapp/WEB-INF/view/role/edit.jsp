<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>编辑角色</title>
</head>
<body id="role-edit">
	<article class="page-container">
		<form action="role/save.do" method="post" class="form form-horizontal" id="role-form">
			<input type="hidden" value="${role.id}" name="id">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${role.name}" placeholder="" id="name" name="name" required="true" maxlength="30">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">排序码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${role.sort}" placeholder="" id="sort" name="sort" number="true" maxlength="9">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">备注：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<textarea name="remark" cols="" rows="" class="textarea" placeholder="说点什么..." onKeyUp="textarealength(this,50)">${role.remark}</textarea>
					<p class="textarea-numberbar">
						<em class="textarea-length">0</em>/50
					</p>
				</div>
			</div>
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>


</body>
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	var $dom = $("#role-edit");
	var $form = $dom.find("#role-form");
	$(function() {

		/**校验*/
		$form.validate({
			onkeyup : false,
			focusCleanup : true,
			success : 'valid',
			submitHandler : function(form) {
				ajaxSubmit($form, "role/save.do", function(_resp) {
					if (_resp.status == '000002') {
						U.alert(_resp.message);
						return;
					}
					U.msg('操作成功', 500, function() {
						var index = window.parent.layer.getFrameIndex(window.name);
						window.parent.load();
						window.parent.layer.close(index);
					});
				});
			}
		});
	});
</script>
</html>