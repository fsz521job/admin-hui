﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>角色管理</title>
</head>
<body id="role-main">
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 人员管理 <span class="c-gray en">&gt;</span> 角色管理 <a id="refresh"
			class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
			class="Hui-iconfont">&#xe68f;</i> </a>
	</nav>


	<div class="page-container" style="width:80%;margin: 0 auto;">
		<div class="text-c">
			<form method="post">
				&nbsp;角色名：&nbsp;<input type="text" class="input-text" style="width:200px" name="name"/>
				<button id="search" type="button" class="btn btn-primary radius">
					<i class="Hui-iconfont">&#xe665;</i>查询
				</button>
				<button type="reset" class="btn btn-success radius">
					<i class="Hui-iconfont">&#xe66c;</i>重置
				</button>
			</form>
		</div>
		<div class="cl pd-5 bg-1 bk-gray mt-20">
			<span class="l"> <a href="javascript:;" id="add" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加角色</a> </span>
		</div>
		<div class="mt-20">
			<table class="table table-border table-bordered table-hover table-bg table-sort">
				<thead>
					<tr class="text-c">
						<th width="25">序号</th>
						<th width="100">角色名</th>
						<th width="250">备注</th>
						<th width="50">排序</th>
						<th width="150">操作</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<div id="layerPage" class="mt-20 text-c"></div>
		</div>
	</div>

</body>
<jsp:include page="../common/script.jsp" />
<script type="text/template">
		<tr class="text-c">
			<td>{{_index}}</td>
			<td>{{name}}</td>
			<td>{{remark}}</td>
			<td>{{sort}}</td>
			<td class="td-manage">
				<a title="授权" href="javascript:;" onclick="grant('{{id}}')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe63c;</i> </a> 
				<a title="编辑" href="javascript:;" onclick="roleEdit('update?id={{id}}')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i> </a> 
				<a title="删除" href="javascript:;" onclick="roleRemove('{{id}}')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i> </a>
			</td>
		</tr>
</script>
<script type="text/javascript">
	var $dom = $("#role-main");
	var $template = $dom.find("[type='text/template']");
	var $dataList = $dom.find("tbody");
	var $queryForm = $dom.find("form");
	var $page = $dom.find("#layerPage");
	$(function() {
		/**加载列表*/
		load();

		/** 添加 */
		$dom.find("#add").click(function() {
			roleEdit("add");
		});

		/**查询*/
		$queryForm.find("#search").click(function() {
			//表单序列化成json对象
			var json = $queryForm.serializeObject();
			U.loadPage($template, $dataList, $page, "role/page.do", json);
		});

	});

	/**加载列表*/
	function load() {
		U.loadPage($template, $dataList, $page, "role/page.do");
	}

	/**编辑*/
	function roleEdit(url) {
		U.showHtmlDialog('角色编辑', url, '', '400')
	}

	/**删除*/
	function roleRemove(id) {
		U.confirm('确认要删除吗？', function() {
			ajaxCall("role/remove.do", {
				"id" : id
			}, function() {
				U.msg('已删除', 500, function() {
					load();
				});
			});
		});
	}

	/**授权*/
	function grant(id) {
		U.showHtmlDialog('授权', "viewGrant?id=" + id, '400', '600')
	}
</script>
</html>