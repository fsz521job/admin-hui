<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ActiveMQ Demo程序</title>
</head>
<body id="message-activemq">
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 消息管理 <span class="c-gray en">&gt;</span> 消息队列 <a id="refresh"
			class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
			class="Hui-iconfont">&#xe68f;</i> </a>
	</nav>

	<div class="page-container">
		<p class="c-red f-24">*必须先开启：1.activemq服务</p>
		<p class="c-primary f-24">发布点对点Queue：</p>
		<div>
			<textarea class="textarea" style="width:95%; height:150px; resize:none"></textarea>
		</div>
		<div class="mt-20 text-c">
			<button id="saveQueue" class="btn btn-success radius" type="submit">
				<i class="icon-ok"></i> 确定
			</button>
		</div>

		<p class="c-primary f-24">发布订阅消息Topic：</p>
		<div>
			<textarea class="textarea" style="width:95%; height:150px; resize:none"></textarea>
		</div>
		<div class="mt-20 text-c">
			<button id="saveTopic" class="btn btn-success radius" type="submit">
				<i class="icon-ok"></i> 确定
			</button>
		</div>
	</div>
</body>
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	var $dom = $("#message-activemq");
	var $textQueue = $dom.find(".textarea").eq(0);
	var $textTopic = $dom.find(".textarea").eq(1);
	$(function() {
		$dom.find("#saveQueue").click(function() {
			if (C.isEmpty($textQueue.val())) {
				U.alert('内容不能为空');
				return;
			}
			ajaxCall("message/queue.do", {
				"text" : $textQueue.val()
			}, function(_rsp) {
				$textQueue.val("");
				U.msg('queue发布成功');
			});
		});

		$dom.find("#saveTopic").click(function() {
			if (C.isEmpty($textTopic.val())) {
				U.alert('内容不能为空');
				return;
			}
			ajaxCall("message/topic.do", {
				"text" : $textTopic.val()
			}, function(_rsp) {
				$textTopic.val("");
				U.msg('topic发布成功');
			});
		});
	});
</script>
</html>