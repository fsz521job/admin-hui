<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>广播</title>
</head>
<body id="message-broadcast">
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 消息管理 <span class="c-gray en">&gt;</span> 广播 <a id="refresh"
			class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
			class="Hui-iconfont">&#xe68f;</i> </a>
	</nav>

	<div class="page-container">
		<p class="c-red f-24">*必须先开启：1.index界面的websocket服务</p>
		<p class="c-primary f-24">输入待群发的广播内容：</p>
		<div>
			<textarea class="textarea" style="width:95%; height:300px; resize:none"></textarea>
		</div>
		<div class="mt-20 text-c">
			<button id="save" class="btn btn-success radius" type="submit">
				<i class="icon-ok"></i> 确定
			</button>
		</div>
	</div>
</body>
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	var $dom = $("#message-broadcast");
	var $text = $dom.find(".textarea");
	$(function() {
		$dom.find("#save").click(function() {
			if (C.isEmpty($text.val())) {
				U.alert('内容不能为空');
				return;
			}
			ajaxCall("message/broadcast.do", {
				"text" : $text.val()
			}, function(_rsp) {
				$text.val("");
				U.msg('群发通知成功');
			});
		});
	});
</script>
</html>