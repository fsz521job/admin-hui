<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>编辑菜单</title>
<style type="text/css">
/*自定义样式*/
ul.ztree {
	margin-top: 10px;
	border: 1px solid #617775;
	background: #f0f6e4;
	width: 220px;
	height: 360px;
	overflow-y: scroll;
	overflow-x: auto;
}
</style>
</head>
<body id="menu_edit">
	<article class="page-container">
		<form action="menu/save.do" method="post" class="form form-horizontal" id="menu_form">
			<input type="hidden" name="id" value="${menu.id}">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>菜单名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<!-- 菜单名不让修改 -->
					<c:choose>
						<c:when test="${empty menu.name}">
							<input type="text" class="input-text" name="name" required="true" maxlength="30">
						</c:when>
						<c:otherwise>
							<input type="text" class="input-text" disabled="disabled" value="${menu.name}" name="name" required="true" maxlength="30">
						</c:otherwise>
					</c:choose>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>父级节点：</label>
				<div class="formControls col-xs-8 col-sm-9 zTreeDemoBackground left">
					<input type="text" class="input-text" size="1" readonly value="${menu.parentName}" onclick="showMenu()" id="parentId" maxlength="30"> <input
						type="hidden" name="parentId" value="${menu.parentId}">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>菜单类型：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box"><select name="type" required="true" class="select">
							<option value="">请选择</option>
							<option value="1">菜单</option>
							<option value="2">按钮</option>
							<option value="3">接口</option>
					</select>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>菜单码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${menu.code}" name="code" required="true" maxlength="30">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">url：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${menu.url}" name="url" maxlength="100">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">图标：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<img id="img" src=""> <input type="hidden" name="icon" value="${menu.icon}">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"></label>
				<div class="formControls col-xs-8 col-sm-9">
					<button id="search" type="button" class="btn btn-success radius">
						<i class="Hui-iconfont">&#xe665;</i>打开图标库
					</button>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">排序码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${menu.sort}" name="sort" number="true" maxlength="9">
				</div>
			</div>
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>

	<!-- ztree下拉框 -->
	<div id="menuContent" class="menuContent" style="display:none; position: absolute;">
		<ul id="treeDemo" class="ztree" style="margin-top:0; width:300px; height: 300px;"></ul>
	</div>


</body>
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	var $dom = $("#menu_edit");
	var $form = $dom.find("#menu_form");
	var $type = $dom.find("[name='type']");
	var $parentName = $dom.find("#parentId");
	var $parentId = $dom.find("[name='parentId']");
	var $search = $dom.find("#search");
	var $icon = $dom.find("#img");

	var zNodes;
	var typeDefault = '${menu.type}';
	var parentIdDefault = '${menu.parentId}';
	var iconDefault = '${menu.icon}';
	$(function() {
		/**打开icon库*/
		$search.on("click", function() {
			U.showHtmlDialog('选择图标', 'icon', '', '600');
		});

		/**回显*/
		if (typeDefault != '') {
			$type.val(typeDefault);
		}
		if (iconDefault != '') {
			$icon.attr("src", ztreeIconPath + iconDefault);
		}

		/**初始化树*/
		ajaxCall("menu/list.do", {
		//"type" : 1
		}, function(_rsp) {
			zNodes = _rsp.data;
			var tree = $.fn.zTree.init($dom.find("#treeDemo"), setting, zNodes);
			tree.expandAll(true);
			//回显选中的节点
			if (parentIdDefault != '') {
				var checkedNode = tree.getNodeByParam("id", parentIdDefault);
				tree.checkNode(checkedNode, true, true);//强制选中该节点
			}
		});

		/**校验*/
		$form.validate({
			rules : {
				parentId : {
					required : true,
				},
				name : {
					required : true,
				},
				code : {
					required : true,
				},
				type : {
					required : true,
				}
			},
			onkeyup : false,
			focusCleanup : true,
			success : 'valid',
			submitHandler : function(form) {
				if ($dom.find("[name='parentId']").val() == '') {
					U.alert('父级节点不能为空');
					return;
				}
				ajaxSubmit($form, "menu/save.do", function(_resp) {
					if (_resp.status == '000002') {
						U.alert(_resp.message);
						return;
					}
					U.msg('操作成功', 500, function() {
						var index = window.parent.layer.getFrameIndex(window.name);
						//重新加载列表
						window.parent.load();
						//重新加载树
						window.parent.loadTree();
						window.parent.layer.close(index);
					});
				});
			}
		});

	});

	/**ztree配置*/
	var setting = {
		check : {
			enable : true,
			chkStyle : "radio",
			radioType : "all"
		},
		view : {
			dblClickExpand : false
		},
		data : {
			simpleData : {
				enable : true
			}
		},
		callback : {
			onClick : function(e, treeId, treeNode) {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				zTree.checkNode(treeNode, !treeNode.checked, null, true);
				return false;
			},
			onCheck : function(e, treeId, treeNode) {
				var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				var nodes = zTree.getCheckedNodes(true);
				var v = nodes[0];//radio数组有且只有一个选中
				$parentName.attr("value", v.name);
				$parentId.attr("value", v.id);
			}
		}
	};

	/**ztree展示菜单*/
	function showMenu() {
		var cityObj = $parentName;
		var cityOffset = $parentName.offset();
		$("#menuContent").css({
			left : cityOffset.left + 'px',
			top : cityOffset.top + cityObj.outerHeight() + 'px'
		}).slideDown("fast");
		$("body").bind("mousedown", onBodyDown);
	}

	/**ztree点其他处收起菜单*/
	function onBodyDown(event) {
		if (!(event.target.id == "parentId" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length > 0)) {
			$("#menuContent").fadeOut("fast");
			$("body").unbind("mousedown", onBodyDown);
		}
	}
</script>
</html>