<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	//获取icon地址
	String path = request.getContextPath();
	String iconPath = path
			+ "/static/H-ui.admin_v3.0/lib/zTree/v3/css/zTreeStyle/img/diy/";
%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>选择图标</title>
</head>
<body>
	<div class="page-container skin-minimal">
	
	
<div class='radio-box'><input type='radio' name='icon'><img val='1.png' src='<%=iconPath%>1.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='10.png' src='<%=iconPath%>10.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='1_close.png' src='<%=iconPath%>1_close.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='1_open.png' src='<%=iconPath%>1_open.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='2.png' src='<%=iconPath%>2.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='3.png' src='<%=iconPath%>3.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='4.png' src='<%=iconPath%>4.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='5.png' src='<%=iconPath%>5.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='6.png' src='<%=iconPath%>6.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='7.png' src='<%=iconPath%>7.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='8.png' src='<%=iconPath%>8.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='9.png' src='<%=iconPath%>9.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='accept.png' src='<%=iconPath%>accept.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='add.png' src='<%=iconPath%>add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application.png' src='<%=iconPath%>application.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_add.png' src='<%=iconPath%>application_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_delete.png' src='<%=iconPath%>application_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_edit.png' src='<%=iconPath%>application_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_error.png' src='<%=iconPath%>application_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_form.png' src='<%=iconPath%>application_form.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_form_add.png' src='<%=iconPath%>application_form_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_form_delete.png' src='<%=iconPath%>application_form_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_form_edit.png' src='<%=iconPath%>application_form_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_form_magnify.png' src='<%=iconPath%>application_form_magnify.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_get.png' src='<%=iconPath%>application_get.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_key.png' src='<%=iconPath%>application_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_view_tile.png' src='<%=iconPath%>application_view_tile.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='application_xp_terminal.png' src='<%=iconPath%>application_xp_terminal.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='attach.png' src='<%=iconPath%>attach.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_add.png' src='<%=iconPath%>award_star_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_bronze_1.png' src='<%=iconPath%>award_star_bronze_1.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_bronze_2.png' src='<%=iconPath%>award_star_bronze_2.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_bronze_3.png' src='<%=iconPath%>award_star_bronze_3.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_delete.png' src='<%=iconPath%>award_star_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_gold_1.png' src='<%=iconPath%>award_star_gold_1.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_gold_2.png' src='<%=iconPath%>award_star_gold_2.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_gold_3.png' src='<%=iconPath%>award_star_gold_3.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_silver_1.png' src='<%=iconPath%>award_star_silver_1.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='award_star_silver_2.png' src='<%=iconPath%>award_star_silver_2.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='basket.png' src='<%=iconPath%>basket.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='basket_add.png' src='<%=iconPath%>basket_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='basket_delete.png' src='<%=iconPath%>basket_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='basket_edit.png' src='<%=iconPath%>basket_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='basket_error.png' src='<%=iconPath%>basket_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='basket_go.png' src='<%=iconPath%>basket_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book.png' src='<%=iconPath%>book.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_add.png' src='<%=iconPath%>book_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_delete.png' src='<%=iconPath%>book_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_edit.png' src='<%=iconPath%>book_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_error.png' src='<%=iconPath%>book_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_go.png' src='<%=iconPath%>book_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_key.png' src='<%=iconPath%>book_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_link.png' src='<%=iconPath%>book_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='book_open.png' src='<%=iconPath%>book_open.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick.png' src='<%=iconPath%>brick.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bricks.png' src='<%=iconPath%>bricks.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick_add.png' src='<%=iconPath%>brick_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick_delete.png' src='<%=iconPath%>brick_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick_edit.png' src='<%=iconPath%>brick_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick_error.png' src='<%=iconPath%>brick_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick_go.png' src='<%=iconPath%>brick_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='brick_link.png' src='<%=iconPath%>brick_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug.png' src='<%=iconPath%>bug.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug_add.png' src='<%=iconPath%>bug_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug_delete.png' src='<%=iconPath%>bug_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug_edit.png' src='<%=iconPath%>bug_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug_error.png' src='<%=iconPath%>bug_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug_go.png' src='<%=iconPath%>bug_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='bug_link.png' src='<%=iconPath%>bug_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building.png' src='<%=iconPath%>building.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_add.png' src='<%=iconPath%>building_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_delete.png' src='<%=iconPath%>building_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_edit.png' src='<%=iconPath%>building_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_error.png' src='<%=iconPath%>building_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_go.png' src='<%=iconPath%>building_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_key.png' src='<%=iconPath%>building_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='building_link.png' src='<%=iconPath%>building_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart.png' src='<%=iconPath%>cart.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_add.png' src='<%=iconPath%>cart_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_delete.png' src='<%=iconPath%>cart_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_edit.png' src='<%=iconPath%>cart_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_error.png' src='<%=iconPath%>cart_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_go.png' src='<%=iconPath%>cart_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_put.png' src='<%=iconPath%>cart_put.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cart_remove.png' src='<%=iconPath%>cart_remove.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd.png' src='<%=iconPath%>cd.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd_add.png' src='<%=iconPath%>cd_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd_burn.png' src='<%=iconPath%>cd_burn.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd_delete.png' src='<%=iconPath%>cd_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd_edit.png' src='<%=iconPath%>cd_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd_eject.png' src='<%=iconPath%>cd_eject.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cd_go.png' src='<%=iconPath%>cd_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_bar.png' src='<%=iconPath%>chart_bar.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_bar_add.png' src='<%=iconPath%>chart_bar_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_bar_delete.png' src='<%=iconPath%>chart_bar_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_bar_edit.png' src='<%=iconPath%>chart_bar_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_bar_error.png' src='<%=iconPath%>chart_bar_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_bar_link.png' src='<%=iconPath%>chart_bar_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve.png' src='<%=iconPath%>chart_curve.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve_add.png' src='<%=iconPath%>chart_curve_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve_delete.png' src='<%=iconPath%>chart_curve_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve_edit.png' src='<%=iconPath%>chart_curve_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve_error.png' src='<%=iconPath%>chart_curve_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve_go.png' src='<%=iconPath%>chart_curve_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_curve_link.png' src='<%=iconPath%>chart_curve_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_line.png' src='<%=iconPath%>chart_line.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_line_add.png' src='<%=iconPath%>chart_line_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_line_delete.png' src='<%=iconPath%>chart_line_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_line_edit.png' src='<%=iconPath%>chart_line_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_line_error.png' src='<%=iconPath%>chart_line_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_line_link.png' src='<%=iconPath%>chart_line_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_pie.png' src='<%=iconPath%>chart_pie.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_pie_add.png' src='<%=iconPath%>chart_pie_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_pie_delete.png' src='<%=iconPath%>chart_pie_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_pie_edit.png' src='<%=iconPath%>chart_pie_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_pie_error.png' src='<%=iconPath%>chart_pie_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='chart_pie_link.png' src='<%=iconPath%>chart_pie_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cog.png' src='<%=iconPath%>cog.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cog_add.png' src='<%=iconPath%>cog_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cog_delete.png' src='<%=iconPath%>cog_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cog_edit.png' src='<%=iconPath%>cog_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cog_error.png' src='<%=iconPath%>cog_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='cog_go.png' src='<%=iconPath%>cog_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comment.png' src='<%=iconPath%>comment.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comments.png' src='<%=iconPath%>comments.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comments_add.png' src='<%=iconPath%>comments_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comments_delete.png' src='<%=iconPath%>comments_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comment_add.png' src='<%=iconPath%>comment_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comment_delete.png' src='<%=iconPath%>comment_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='comment_edit.png' src='<%=iconPath%>comment_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer.png' src='<%=iconPath%>computer.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_add.png' src='<%=iconPath%>computer_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_delete.png' src='<%=iconPath%>computer_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_edit.png' src='<%=iconPath%>computer_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_error.png' src='<%=iconPath%>computer_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_go.png' src='<%=iconPath%>computer_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_key.png' src='<%=iconPath%>computer_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='computer_link.png' src='<%=iconPath%>computer_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group.png' src='<%=iconPath%>group.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_add.png' src='<%=iconPath%>group_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_delete.png' src='<%=iconPath%>group_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_edit.png' src='<%=iconPath%>group_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_error.png' src='<%=iconPath%>group_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_gear.png' src='<%=iconPath%>group_gear.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_go.png' src='<%=iconPath%>group_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_key.png' src='<%=iconPath%>group_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='group_link.png' src='<%=iconPath%>group_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='key.png' src='<%=iconPath%>key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='key_add.png' src='<%=iconPath%>key_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='key_delete.png' src='<%=iconPath%>key_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='key_go.png' src='<%=iconPath%>key_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page.png' src='<%=iconPath%>page.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_add.png' src='<%=iconPath%>page_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_attach.png' src='<%=iconPath%>page_attach.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_code.png' src='<%=iconPath%>page_code.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_copy.png' src='<%=iconPath%>page_copy.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_delete.png' src='<%=iconPath%>page_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_edit.png' src='<%=iconPath%>page_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_error.png' src='<%=iconPath%>page_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_excel.png' src='<%=iconPath%>page_excel.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_find.png' src='<%=iconPath%>page_find.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_gear.png' src='<%=iconPath%>page_gear.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_go.png' src='<%=iconPath%>page_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_green.png' src='<%=iconPath%>page_green.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_key.png' src='<%=iconPath%>page_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_lightning.png' src='<%=iconPath%>page_lightning.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_link.png' src='<%=iconPath%>page_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_paintbrush.png' src='<%=iconPath%>page_paintbrush.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_paste.png' src='<%=iconPath%>page_paste.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_red.png' src='<%=iconPath%>page_red.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_refresh.png' src='<%=iconPath%>page_refresh.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='page_save.png' src='<%=iconPath%>page_save.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='photo.png' src='<%=iconPath%>photo.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='photos.png' src='<%=iconPath%>photos.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='photo_add.png' src='<%=iconPath%>photo_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='photo_delete.png' src='<%=iconPath%>photo_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='photo_link.png' src='<%=iconPath%>photo_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture.png' src='<%=iconPath%>picture.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='pictures.png' src='<%=iconPath%>pictures.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_add.png' src='<%=iconPath%>picture_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_delete.png' src='<%=iconPath%>picture_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_edit.png' src='<%=iconPath%>picture_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_empty.png' src='<%=iconPath%>picture_empty.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_error.png' src='<%=iconPath%>picture_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_go.png' src='<%=iconPath%>picture_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_key.png' src='<%=iconPath%>picture_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_link.png' src='<%=iconPath%>picture_link.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='picture_save.png' src='<%=iconPath%>picture_save.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table.png' src='<%=iconPath%>table.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_add.png' src='<%=iconPath%>table_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_delete.png' src='<%=iconPath%>table_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_edit.png' src='<%=iconPath%>table_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_error.png' src='<%=iconPath%>table_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_gear.png' src='<%=iconPath%>table_gear.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_go.png' src='<%=iconPath%>table_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='table_key.png' src='<%=iconPath%>table_key.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit.png' src='<%=iconPath%>transmit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit_add.png' src='<%=iconPath%>transmit_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit_blue.png' src='<%=iconPath%>transmit_blue.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit_delete.png' src='<%=iconPath%>transmit_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit_edit.png' src='<%=iconPath%>transmit_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit_error.png' src='<%=iconPath%>transmit_error.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='transmit_go.png' src='<%=iconPath%>transmit_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user.png' src='<%=iconPath%>user.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_add.png' src='<%=iconPath%>user_add.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_comment.png' src='<%=iconPath%>user_comment.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_delete.png' src='<%=iconPath%>user_delete.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_edit.png' src='<%=iconPath%>user_edit.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_female.png' src='<%=iconPath%>user_female.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_go.png' src='<%=iconPath%>user_go.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_gray.png' src='<%=iconPath%>user_gray.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_green.png' src='<%=iconPath%>user_green.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_orange.png' src='<%=iconPath%>user_orange.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_red.png' src='<%=iconPath%>user_red.png'></div>
<div class='radio-box'><input type='radio' name='icon'><img val='user_suit.png' src='<%=iconPath%>user_suit.png'></div>



	</div>
</body>
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	$(function() {
		/**radio皮肤*/
		$('.skin-minimal input').iCheck({
			checkboxClass : 'icheckbox-blue',
			radioClass : 'iradio-blue',
			increaseArea : '20%'
		});

		/**给父页面赋值*/
		$("input[name='icon']").change(function(){
			//注意：框架自动生成了其他节点，next()有变！！
			//$("#父窗口元素ID",window.parent.document).find("#man_id").val();
			var $parentDom=$("#img", window.parent.document);
			$parentDom.attr("src",$(this).parent().next().attr("src"));
			$parentDom.next().val($(this).parent().next().attr("val"));
			var index = window.parent.layer.getFrameIndex(window.name);
			window.parent.layer.close(index);
		});
		
	});
</script>
</html>