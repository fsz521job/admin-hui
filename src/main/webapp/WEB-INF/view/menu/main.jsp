﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- apache shiro 权限标签 -->
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>菜单管理</title>
</head>
<body id="menu-main" class="pos-r">
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 人员管理 <span class="c-gray en">&gt;</span> 菜单管理 <a id="refresh"
			class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
			class="Hui-iconfont">&#xe68f;</i> </a>
	</nav>
	<div class="pos-a" style="width:250px;margin-top:40px;left:0;top:0; bottom:0; height:100%; border-right:1px solid #e5e5e5; background-color:#f5f5f5">
		<ul id="menuTree" class="ztree">
		</ul>
	</div>
	<div style="margin-left:250px;">
		<div class="page-container">
			<div class="text-c">
				<form method="post">
					&nbsp;菜单名：&nbsp;<input type="text" class="input-text" style="width:100px" name="name"/> &nbsp;菜单码：&nbsp;<input type="text"
						class="input-text" style="width:100px" name="code"/> &nbsp;类型：&nbsp;<span class="select-box inline"><select name="type"
						class="select" style="width:100px">
							<option value="">请选择</option>
							<option value="1">菜单</option>
							<option value="2">按钮</option>
							<option value="3">接口</option>
					</select> </span>
					<button id="search" type="button" class="btn btn-primary radius">
						<i class="Hui-iconfont">&#xe665;</i>查询
					</button>
					<button type="reset" class="btn btn-success radius">
						<i class="Hui-iconfont">&#xe66c;</i>重置
					</button>
				</form>
			</div>
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l"> 
					<shiro:hasPermission name="menu:add">
						<a href="javascript:;" id="menuEdit" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加菜单</a> 
					</shiro:hasPermission>
				</span>
			</div>
			<div class="mt-20">
				<table class="table table-border table-bordered table-hover table-bg table-sort">
					<thead>
						<tr class="text-c">
							<th width="30">序号</th>
							<th width="100">菜单名</th>
							<th width="150">菜单代码</th>
							<th width="50">类型</th>
							<th width="50">排序</th>
							<th width="250">url</th>
							<th width="50">图标</th>
							<th width="100">操作</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="layerPage" class="mt-20 text-c"></div>
			</div>
		</div>

	</div>

</body>
<jsp:include page="../common/script.jsp" />
<script type="text/template">
		<tr class="text-c">
			<td>{{_index}}</td>
			<td>{{name}}</td>
			<td>{{code}}</td>
			<td>{{_type}}</td>
			<td>{{sort}}</td>
			<td>{{url}}</td>
			<td><img src="{{_icon}}"></td>
			<td class="td-manage" _id="{{id}}" >
				{{_content}}
			</td>
		</tr>
</script>
<script type="text/javascript">
	var $dom = $("#menu-main");
	var $template = $dom.find("[type='text/template']");
	var $dataList = $dom.find("tbody");
	var $queryForm = $dom.find("form");
	var $page = $dom.find("#layerPage");

	var zNodes;
	$(function() {
		/**加载树*/
		loadTree();

		/**加载列表*/
		load();

		/** 添加 */
		$dom.find("#menuEdit").click(function() {
			menuEdit("add");
		});

		/**查询*/
		$queryForm.find("#search").click(function() {
			//表单序列化成json对象
			var json = $queryForm.serializeObject();
			load(json);
		});

		/**修改*/
		$dom.on("click", "#edit", function() {
			menuEdit("update?id="+$(this).parent().attr("_id"));
		});

		/**删除*/
		$dom.on("click", "#remove", function() {
			menuRemove($(this).parent().attr("_id"));
		});

	});

	/**加载列表*/
	function load(param) {
		param = param || {};
		U.loadPage($template, $dataList, $page, "menu/page.do", param, filter);
	}

	/**自定义模板*/
	function filter(obj) {
		if (obj.type == 1) {
			obj._type = '菜单';
		} else if (obj.type == 2) {
			obj._type = '按钮';
		} else if (obj.type == 3) {
			obj._type = '接口';
		}
		//菜单icon
		obj._icon = ztreeIconPath + obj.icon;
		//是否root
		if (obj.parentId != null) {
			obj._content = "<shiro:hasPermission name='menu:update'><a id='edit' title='编辑' href='javascript:;' class='ml-5' style='text-decoration:none'><i class='Hui-iconfont'>&#xe6df;</i> </a></shiro:hasPermission> "
					+ "<shiro:hasPermission name='menu:delete'><a id='remove' title='删除' href='javascript:;' class='ml-5' style='text-decoration:none'><i class='Hui-iconfont'>&#xe6e2;</i> </a></shiro:hasPermission>";
		}
	}

	/**编辑*/
	function menuEdit(url) {
		U.showHtmlDialog('菜单编辑', url, '', '600',true);
	}

	/**删除*/
	function menuRemove(id) {
		U.confirm('确认要删除吗？', function() {
			ajaxCall("menu/remove.do", {
				"id" : id
			}, function() {
				U.msg('已删除', 500, function() {
					load();
					loadTree();
				});
			});
		});
	}

	/**加载树*/
	function loadTree() {
		ajaxCall("menu/list.do", {
			//"type" : 1
		}, function(_rsp) {
			zNodes = _rsp.data;
			var tree=$.fn.zTree.init($dom.find("#menuTree"), setting, zNodes);
			tree.expandAll(true);
			//树加载完成后模拟点击第一个节点 
			//$dom.find("#menuTree a:first").trigger("click");
		});
	}

	/**ztree配置*/
	var setting = {
		view : {
			selectedMulti : false
		},
		data : {
			simpleData : {
				enable : true,
			}
		},
		callback : {
			onClick : function(event, treeId, treeNode, clickFlag) {
				load({
					"parentId" : treeNode.id
				});
			}
		}
	};
</script>
</html>