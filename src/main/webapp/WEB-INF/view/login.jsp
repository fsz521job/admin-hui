﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<!-- 标识pageIdentity用来判断页面属于什么模块 -->
<html pageIdentity="login">
<head>
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico" media="screen" />
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<script type="text/javascript" src="lib/PIE_IE678.js"></script>
<![endif]-->
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/css/common.css" rel="stylesheet" type="text/css" />
<!--[if IE 6]>
<script type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>后台登录</title>
</head>
<body>
	<div class="header">
		<lable class="f-30 c-999 ml-20 ">admin-hui后台管理系统 V1.0</lable>
	</div>
	<div class="loginWraper">
		<div id="loginform" class="loginBox">
			<form class="form form-horizontal" action="" method="post">
				<div class="row cl">
					<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i> </label>
					<div class="formControls col-xs-8">
						<input id="" name="username" value="admin" type="text" placeholder="用户" class="input-text radius size-L" required="true" rangelength="[4,10]">
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i> </label>
					<div class="formControls col-xs-8">
						<input id="" name="password" value="123456" type="password" placeholder="密码" class="input-text radius size-L" required="true">
					</div>
				</div>
				<div class="row cl">
					<div class="formControls col-xs-8 col-xs-offset-3">
						<input class="input-text radius size-L" name="captcha" type="text" placeholder="验证码" required="true" style="width:150px;"> <img
							src="${pageContext.request.contextPath}/captcha"> <a id="kanbuq" href="javascript:;">看不清，换一张</a>
					</div>
				</div>
				<div class="row cl">
					<div class="formControls col-xs-8 col-xs-offset-3">
						<label for="online"> <input type="checkbox" name="remember" id="online" value="true"> 记住我7天之内免登录</label>
					</div>
				</div>
				<div class="my-notice">
					<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> <span id="tips-notice"></span>
				</div>
				<div class="formControls col-xs-8 col-xs-offset-3">
					<input id="submit" type="button" class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;"> <input
						style="margin-left: 50px" name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;重&nbsp;&nbsp;&nbsp;&nbsp;置&nbsp;">
				</div>
			</form>
		</div>
	</div>
	<div class="footer">Copyright xxx by Chensi</div>
</body>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.setting.js"></script>
<script type="text/javascript">
	var $dom = $("#loginform");
	var $notice = $("#tips-notice");
	var $form = $("form:eq(0)");
	var $captcha = $dom.find("[name='captcha']");
	var isFirst = true;
	var isCheckedCaptcha = false;
	$(function() {
		/**防止页面嵌套*/
		if (top.location != self.location) {
			top.location = self.location;
		}

		/**更换验证码*/
		$dom.find("#kanbuq").click(function() {
			$(this).prev().hide().attr('src', '${pageContext.request.contextPath}/captcha?' + Math.floor(Math.random() * 100000)).fadeIn();
			//重置验证码判断
			isCheckedCaptcha = false;
		});

		$captcha.blur(function() {
			if ($captcha.val() == '') {
				$notice.html('验证码输入有误, 该项为必填项');
				$notice.show();
				isCheckedCaptcha = false;
				return;
			}
			ajaxCall("checkCaptcha.do", {
				"captcha" : $captcha.val()
			}, function(_resp) {
				if (_resp.status == '000002') {
					$notice.html(_resp.message);
					$notice.show();
					isCheckedCaptcha = false;
					return;
				}
				if ($notice.html().indexOf('验证码错误') != -1) {
					$notice.hide();
				}
				isCheckedCaptcha = true;
			});
		});

		// 校验表单
		$form.validate({
			onfocusout : false,
			onkeyup : false,
			focusInvalid : false,
			errorPlacement : function(error, element) {
				if (isFirst) {
					//定位错误
					var elementName = element.attr("placeholder");
					var errorMessage = '输入有误, ' + error.html();
					$notice.html(elementName + errorMessage);
					$notice.show();
					isFirst = false;
				}
			},
			submitHandler : function(form) {
				if (!isCheckedCaptcha) {
					$notice.html('验证码错误');
					$notice.show();
					return;
				}
				$notice.hide();
				ajaxSubmit($form, "login.do", function(_resp) {
					if (_resp.status == '000002') {
						$notice.html(_resp.message);
						$notice.show();
						$dom.find("[type='reset']").trigger("click");
						return;
					}
					var loadingIndex = U.loading();
					//停顿一秒进入主页面
					window.setTimeout(function() {
						//存储当前用户数据
						window.sessionStorage.setItem("loginUser", _resp.data.id);
						window.location = app_path + "/index.html";
					}, 1000);

				});
			}
		});

		// 提交按钮
		$dom.find("#submit").click(function() {
			isFirst = true;
			$form.submit();
		});
	});
</script>
</html>