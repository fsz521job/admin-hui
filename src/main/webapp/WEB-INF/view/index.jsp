﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico" media="screen" />
<jsp:include page="common/head.jsp" />
<title>后台管理系统</title>
<style type="text/css">
/**保持无限极菜单对齐*/
li dl {
	padding-left: 10px
}
</style>
</head>
<body>
	<!-- 顶部导航栏 -->
	<header class="navbar-wrapper">
		<div class="navbar navbar-fixed-top">
			<div class="container-fluid cl">
				<lable class="logo navbar-logo f-l mr-10 hidden-xs">后台管理系统</lable>
				<span class="logo navbar-slogan f-l mr-10 hidden-xs">v1.0</span>
				<nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
					<ul class="cl">
						<li>欢迎您</li>
						<li class="dropDown dropDown_hover"><a href="#" class="dropDown_A">${sessionScope.loginUser.name} <i class="Hui-iconfont">&#xe6d5;</i> </a>
							<ul class="dropDown-menu menu radius box-shadow">
								<li><a id="index-userDetail" href="javascript:;">个人信息</a>
								</li>
								<li><a href="javascript:;">切换账户</a>
								</li>
								<li><a id="logout" href="javascript:;">退出</a>
								</li>
							</ul>
						</li>
						<li id="Hui-msg"><a href="javascript:;" title="消息" onclick="notice();"><span class="badge badge-danger">1</span><i class="Hui-iconfont"
								style="font-size:18px">&#xe68a;</i> </a>
						</li>
						<li id="Hui-skin" class="dropDown right dropDown_hover"><a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont"
								style="font-size:18px">&#xe62a;</i> </a>
							<ul class="dropDown-menu menu radius box-shadow">
								<li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a>
								</li>
								<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a>
								</li>
								<li><a href="javascript:;" data-val="green" title="绿色">绿色</a>
								</li>
								<li><a href="javascript:;" data-val="red" title="红色">红色</a>
								</li>
								<li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a>
								</li>
								<li><a href="javascript:;" data-val="orange" title="绿色">橙色</a>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</header>


	<!-- 左侧菜单栏 -->
	<aside class="Hui-aside">
		<input runat="server" id="divScrollValue" type="hidden" value="" />
		<div id="menu" class="menu_dropdown bk_2">${loginMenu}</div>
	</aside>


	<!-- 左侧菜单-隐藏显示 -->
	<div class="dislpayArrow hidden-xs">
		<a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a>
	</div>


	<!-- 主体内容 -->
	<section class="Hui-article-box">
		<div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
			<div class="Hui-tabNav-wp">
				<ul id="min_title_list" class="acrossTab cl">
					<li class="active"><span title="我的桌面" data-href="viewWelcome">我的桌面</span><em></em>
					</li>
				</ul>
			</div>
			<div class="Hui-tabNav-more btn-group">
				<a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i> </a><a id="js-tabNav-next"
					class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i> </a>
			</div>
		</div>
		<div id="iframe_box" class="Hui-article">
			<div class="show_iframe">
				<div style="display:none" class="loading"></div>
				<iframe scrolling="yes" frameborder="0" src="viewWelcome"></iframe>
			</div>
		</div>
	</section>
	
	<div class="contextMenu" id="Huiadminmenu">
		<ul>
			<li id="closethis">关闭当前 </li>
			<li id="closeall">关闭全部 </li>
		</ul>
	</div>
	

	<footer class="footer mt-20">
		<div class="container-fluid">
			<nav>
				<a href="#" target="_blank">关于我们</a> <span class="pipe">|</span> <a href="#" target="_blank">联系我们</a> <span class="pipe">|</span> <a href="#"
					target="_blank">法律声明</a>
			</nav>
			<p>
				Copyright &copy;2016 H-ui.net All Rights Reserved. <br> <a href="http://www.miitbeian.gov.cn/" target="_blank" rel="nofollow">京ICP备1000000号</a><br>
			</p>
		</div>
	</footer>
</body>
<jsp:include page="common/script.jsp" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/layui/css/layui.css" media="all">
<script type="text/javascript" src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script type="text/javascript">
	var $loginName = $("#Hui-userbar").children().find(".dropDown_A").eq(0);
	var $menu = $("#menu");
	var $notice = $("#notice");
	$(function() {
		
		/**防止页面嵌套*/
		if (top.location != self.location) {
			top.location = self.location;
		}

		/**注销*/
		$("#logout").click(function() {
			U.confirm("确认要退出吗？", function() {
				ajaxCall("logout.do");
			});
		});
		
		/**查询用户详情*/
		$("#index-userDetail").click(function() {
			U.showHtmlDialog('用户信息', "user/detail?id="+${sessionScope.loginUser.id}, '800', '500');
		});
		
		/**接收未读消息*/
		setTimeout(function() {
			notice();
		}, 1000);
		
	});
	
	/**消息提醒*/
	function notice(){
		U.notice("您有待办任务，请及时处理");
	}
	
	/**监听消息推送*/
	function listenMsg(data){
		//监听消息
		if (data.emit == 'notice') {
			U.notice(data.content); 
			return;
		}
		//监听超时
		if (data.emit == 'overtime') {
			U.overtime("您已超时，请重新登录",function(){
				window.location = app_path + '/login.html';
			}); 
			return;
		}
	}
</script>
</html>