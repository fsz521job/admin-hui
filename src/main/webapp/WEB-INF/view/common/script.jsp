<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	//js版本，防止浏览器缓存js
	String version = new Date().getTime()+"";
%>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js?<%=version%>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.form.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.setting.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/uploadify/jquery.uploadify.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui/js/H-ui.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui.admin/js/H-ui.admin.js"></script>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/icheck/jquery.icheck.min.js"></script>--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/Validform/5.3.2/Validform.min.js"></script>--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/jquery.validation/1.14.0/validate-methods.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/jquery.validation/1.14.0/messages_zh.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/zTree/v3/js/jquery.ztree.all-3.5.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/jquery.contextmenu/jquery.contextmenu.r2.js"></script>
