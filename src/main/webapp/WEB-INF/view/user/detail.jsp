<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>用户查看</title>
</head>
<body>
<div class="cl pd-20" style=" background-color:#5bacb6">
  <img class="avatar size-XXL l" src="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui/images/user.jpg">
  <dl style="margin-left:150px; color:#fff">
    <dt><span class="f-18">${user.username}</span> <span class="pl-10 f-12">余额：40</span></dt>
    <dd class="pt-10 f-12" style="margin-left:0">这家伙很懒，什么也没有留下</dd>
  </dl>
</div>
<div class="pd-20">
  <table class="table">
    <tbody>
      <tr>
        <th class="text-r" width="80">姓名：</th>
        <td>${user.name}</td>
      </tr>
      <tr>
        <th class="text-r">手机：</th>
        <td>${user.tel}</td>
      </tr>
      <tr>
        <th class="text-r">邮箱：</th>
        <td>${user.email}</td>
      </tr>
      <tr>
        <th class="text-r">地址：</th>
        <td>${user.address}</td>
      </tr>
      <tr>
        <th class="text-r">注册时间：</th>
        <td>${user.createTime}</td>
      </tr>
    </tbody>
  </table>
</div>
</body>
<jsp:include page="../common/script.jsp" />
</html>