<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>编辑用户</title>
</head>
<body id="user-edit">
	<article class="page-container">
		<form action="user/save.do" method="post" class="form form-horizontal" id="form-user">
			<input type="hidden" value="${user.id}" name="id">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>用户名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.username}" placeholder="" id="username" name="username" required="true" maxlength="30">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>姓名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.name}" placeholder="" id="name" name="name" required="true" maxlength="30">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box"> <select class="select" size="1" id="roleId" name="roleId" required="true">
							<option value="">请选择</option>
					</select> </span>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>手机：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.tel }" placeholder="" id="tel" name="tel" required="true" isMobile="true" maxlength="30">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>邮箱：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.email }" placeholder="@" name="email" id="email" required="true" email="true" maxlength="50">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">openid：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.openid }" name="openid" id="openid" maxlength="50">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">省：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box" style="width:25%"> <select class="select" size="1" id="province" name="province">
							<option value=''>请选择</option>
					</select> </span> <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;市：</label> <span class="select-box" style="width:25%"> <select class="select" size="1" id="city"
						name="city">
							<option value=''>请选择</option>
					</select> </span> <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;区：</label> <span class="select-box" style="width:25%"> <select class="select" size="1" id="area"
						name="area">
							<option value=''>请选择</option>
					</select> </span>
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">地址：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.address }" name="address" id="address" maxlength="50">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">身份证号：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.idcard }" name="idcard" id="idcard" maxlength="50">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">签名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.sign }" name="sign" id="sign" maxlength="30">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">头像：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<img style="width:150px;height:150px" src="${pageContext.request.contextPath}/images/pic.png" onerror="this.src='${pageContext.request.contextPath}/images/nopic.png'"> <input type="hidden" value="${user.avatar}" name="avatar">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"></label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="file" name="file2" id="upload2" />
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">附件：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="${user.attach}" name="attach" readonly="readonly">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"></label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="file" name="file1" id="upload1" />
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">备注：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<textarea name="remark" cols="" rows="" class="textarea" placeholder="说点什么..." onKeyUp="textarealength(this,100)">${user.remark}</textarea>
					<p class="textarea-numberbar">
						<em class="textarea-length">0</em>/100
					</p>
				</div>
			</div>
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>


</body>
<!-- 注意  uploadify插件js部分必须在最底下加载 -->
<jsp:include page="../common/script.jsp" />
<script type="text/javascript">
	var $dom = $("#user-edit");
	var $form = $dom.find("#form-user");
	var $province = $dom.find("#province");
	var $city = $dom.find("#city");
	var $area = $dom.find("#area");
	var $role = $dom.find("#roleId");
	var $upload1 = $dom.find("#upload1");
	var $upload2 = $dom.find("#upload2");

	var provinceDefault = '${user.province}';
	var cityDefault = '${user.city}';
	var areaDefault = '${user.area}';
	var roleIdDefault = '${user.roleId}';
	var avatarDefault = '${user.avatar}';

	$(function() {
		/**回显头像*/
		if (avatarDefault != '') {
			$dom.find("[name='avatar']").prev().attr("src", "${pageContext.request.contextPath}/" + avatarDefault);
		}

		/**加载省市区*/
		U.loadPCA($province, $city, $area, provinceDefault, cityDefault, areaDefault);

		/**加载角色*/
		U.initSelect({
			url : "role/list.do",
			$dom : $role,
			callback : function() {
				if (roleIdDefault != '') {
					$role.val(roleIdDefault);
				}
			}
		});

		/**校验*/
		$form.validate({
			onkeyup : false,
			focusCleanup : true,
			success : 'valid',
			submitHandler : function(form) {
				ajaxSubmit($form, "user/save.do", function(_resp) {
					if (_resp.status == '000002') {
						U.alert(_resp.message);
						return;
					}
					//$(parent.document).find("#refresh").children().trigger("click");//此方法會刷新页面，体验不好
					U.msg('操作成功', 500, function() {
						var index = window.parent.layer.getFrameIndex(window.name);
						window.parent.load();
						window.parent.layer.close(index);
					});
				});
			}
		});

		/**上传*/
		$upload1.uploadify({
			'height' : 25,
			'width' : 80,
			//按钮显示的文字
			'buttonText' : '浏览上传',
			//指定swf文件
			'swf' : '${pageContext.request.contextPath}/static/uploadify/uploadify.swf',
			//上传接口
			'uploader' : '${pageContext.request.contextPath}/upload.do',
			//取消上传图片
			'cancelImg' : '${pageContext.request.contextPath}/static/uploadify/uploadify-cancel.png',
			//选择文件后自动上传
			'auto' : true,
			//设置为true将允许多文件上传
			'multi' : false,
			//file input元素是有name属性的，但是使用flash后需要手动传递过去，也就是fileObjName这个属性
			//'fileObjName':'attach',
			//文件大小
			'fileSizeLimit' : '500MB',
			//并发上传数据
			'simUploadLimit' : 3,
			//设置指定后缀名，windows文件浏览会过滤掉其他格式的文件
			'fileTypeExts' : '*.gif; *.jpg; *.png;',
			//发送给后台的其他参数通过formData指定
			'formData' : {},
			//开始上传事件
			'onUploadStart' : function(file) {
			},
			//上传成功回调事件
			'onUploadSuccess' : function(file, data, response) {
				var obj = eval('(' + data + ')');
				if (obj.status == '000000') {
					$dom.find("[name='attach']").val(obj.data);
				}
			},
			//重写默认的英文提示: 指定的事件,指定的事件
			'overrideEvents' : [ 'onSelectError', 'onDialogClose' ],
			//返回一个错误，选择文件的时候触发,注意添加'overrideEvents'选项，要不默认的错误此时还是会出现
			'onSelectError' : function(file, errorCode, errorMsg) {
				switch (errorCode) {
				case -110:
					alert('文件大小超出系统限制');
					break;
				}
			},
			'onError' : function(event, ID, fileObj, errorObj) {
				alert(errorObj.type + ', Error: ' + errorObj.info);
			}
		});

		$upload2.uploadify({
			'height' : 25,
			'width' : 80,
			//按钮显示的文字
			'buttonText' : '浏览上传',
			//指定swf文件
			'swf' : '${pageContext.request.contextPath}/static/uploadify/uploadify.swf',
			//上传接口
			'uploader' : '${pageContext.request.contextPath}/upload.do',
			//取消上传图片
			'cancelImg' : '${pageContext.request.contextPath}/static/uploadify/uploadify-cancel.png',
			//选择文件后自动上传
			'auto' : true,
			//设置为true将允许多文件上传
			'multi' : false,
			//文件大小
			'fileSizeLimit' : '500MB',
			//并发上传数据
			'simUploadLimit' : 3,
			//设置指定后缀名，windows文件浏览会过滤掉其他格式的文件
			'fileTypeExts' : '*.gif; *.jpg; *.png;',
			//发送给后台的其他参数通过formData指定
			'formData' : {},
			//开始上传事件
			'onUploadStart' : function(file) {
			},
			//上传成功回调事件
			'onUploadSuccess' : function(file, data, response) {
				var obj = eval('(' + data + ')');
				if (obj.status == '000000') {
					$dom.find("[name='avatar']").val(obj.data);
					$dom.find("[name='avatar']").prev().attr("src", "${pageContext.request.contextPath}/" + obj.data);
				}
			},
			//重写默认的英文提示: 指定的事件,指定的事件
			'overrideEvents' : [ 'onSelectError', 'onDialogClose' ],
			//返回一个错误，选择文件的时候触发,注意添加'overrideEvents'选项，要不默认的错误此时还是会出现
			'onSelectError' : function(file, errorCode, errorMsg) {
				switch (errorCode) {
				case -110:
					alert('文件大小超出系统限制');
					break;
				}
			},
			'onError' : function(event, ID, fileObj, errorObj) {
				alert(errorObj.type + ', Error: ' + errorObj.info);
			}
		});

	});
</script>
</html>