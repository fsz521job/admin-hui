﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- apache shiro 权限标签 -->
<%@ taglib uri="http://shiro.apache.org/tags" prefix="shiro"%>
<!DOCTYPE HTML>
<html>
<head>
<jsp:include page="../common/head.jsp" />
<title>用户管理</title>
</head>
<body id="user-main">
	<nav class="breadcrumb">
		<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 人员管理 <span class="c-gray en">&gt;</span> 用户管理 <a id="refresh"
			class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新"><i
			class="Hui-iconfont">&#xe68f;</i> </a>
	</nav>


	<div class="page-container">
		<div class="text-c">
			<form method="post">
				&nbsp;用户名：&nbsp;<input type="text" class="input-text" style="width:100px" name="username" /> &nbsp;角色：&nbsp;<span class="select-box inline"><select
					name="roleId" class="select" style="width:100px">
						<option value="">请选择</option>
				</select> </span> &nbsp;状态：&nbsp;<span class="select-box inline"><select name="status" class="select" style="width:100px">
						<option value="">请选择</option>
						<option value="0">正常</option>
						<option value="1">待审核</option>
						<option value="2">冻结</option>
						<option value="10">已注销</option>
				</select> </span>
				<button id="search" type="button" class="btn btn-primary radius">
					<i class="Hui-iconfont">&#xe665;</i>查询
				</button>
				<button type="reset" class="btn btn-success radius">
					<i class="Hui-iconfont">&#xe66c;</i>重置
				</button>
			</form>
		</div>
		<div class="cl pd-5 bg-1 bk-gray mt-20">
			<span class="l"> <shiro:hasPermission name="user:deleteAll">
					<a href="javascript:;" id="removeAll" class="btn btn-danger radius"> <i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>
				</shiro:hasPermission> <shiro:hasPermission name="user:add">
					<a href="javascript:;" id="addUser" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加用户</a>
				</shiro:hasPermission> </span>
		</div>
		<div class="mt-20">
			<table class="table table-border table-bordered table-hover table-bg table-sort radius">
				<thead>
					<tr class="text-c">
						<th width="25"><input type="checkbox" />
						</th>
						<th width="30">序号</th>
						<th width="100">用户名</th>
						<th width="150">角色名</th>
						<th width="100">姓名</th>
						<th width="100">电话</th>
						<th width="150">邮箱</th>
						<th width="150">上次登录</th>
						<th width="50">状态</th>
						<th width="150">操作</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
			<div id="layerPage" class="mt-20 text-c"></div>
		</div>
	</div>
</body>
<jsp:include page="../common/script.jsp" />
<script type="text/template">
		<tr class="text-c">
			<td><input type="checkbox" value="{{id}}"></td>
			<td>{{_index}}</td>
			<td><u style="cursor:pointer;text-decoration:none;" class="text-primary c-primary f-14" onclick="userDetail('{{id}}')">{{username}}</u></td>
			<td>{{roleName}}</td>
			<td>{{name}}</td>
			<td>{{tel}}</td>
			<td>{{email}}</td>
			<td>{{lastTime}}</td>
			<td class="td-status">
				<span class="label label-{{_color}} radius">{{_msg}}</span>
			</td>
			<td class="td-manage">
				<shiro:hasPermission name="user:toFrozen">
					<a title="冻结" href="javascript:;" onClick="changeStatus(this,2,'{{id}}')" class="ml-5" style="text-decoration:none" ><i class="Hui-iconfont">&#xe631;</i></a>
				</shiro:hasPermission>
				<shiro:hasPermission name="user:toNormal">
					<a title="正常" href="javascript:;" onClick="changeStatus(this,0,'{{id}}')" class="ml-5" style="text-decoration:none" ><i class="Hui-iconfont">&#xe6e1;</i></a> 
				</shiro:hasPermission>
				<shiro:hasPermission name="user:update">
					<a title="编辑" href="javascript:;" onclick="userEdit('update?id={{id}}')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i> </a> 
				</shiro:hasPermission>
				<shiro:hasPermission name="user:resetPwd">
					<a title="重置密码" href="javascript:;" onClick="resetPwd('{{id}}')" class="ml-5"  style="text-decoration:none" ><i class="Hui-iconfont">&#xe63f;</i> </a> 
				</shiro:hasPermission>
				<shiro:hasPermission name="user:delete">
					<a title="删除" href="javascript:;" onclick="userRemove('{{id}}')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i> </a>
				</shiro:hasPermission>
			</td>
		</tr>
</script>
<script type="text/javascript">
	var $dom = $("#user-main");
	var $template = $dom.find("[type='text/template']");
	var $dataList = $dom.find("tbody");
	var $queryForm = $dom.find("form");
	var $page = $dom.find("#layerPage");
	var $roleList = $dom.find("[name='roleId']");
	/**初始化*/
	$(function() {
		/**加载列表*/
		load();

		/** 添加用户 */
		$dom.find("#addUser").click(function() {
			userEdit("add");
		});

		/**加载角色*/
		U.initSelect({
			url : "role/list.do",
			$dom : $roleList
		});

		/**查询*/
		$queryForm.find("#search").click(function() {
			//表单序列化成json对象
			var json = $queryForm.serializeObject();
			load(json);
		});

		/**批量删除*/
		$dom.find("#removeAll").click(function() {
			var checkedDoms = $dataList.find("input:checked");
			// 判断是否至少选择一项 
			var checkedNum = checkedDoms.length;
			if (checkedNum == 0) {
				U.alert('请选择至少一项！');
				return;
			}
			U.confirm('确认要删除吗？', function() {
				var checkedList;
				var arr = new Array();
				checkedDoms.each(function() {
					arr.push($(this).val());
				});
				checkedList = arr + "";//springmvc识别字符串数组
				ajaxCall("user/removeAll.do", {
					"ids" : checkedList
				}, function() {
					U.msg('已删除', 500, function() {
						load();
					});
				});
			});
		});
	});

	/**加载列表*/
	function load(param) {
		param = param || {};
		U.loadPage($template, $dataList, $page, "user/page.do", param, filter);
	}

	/**template模板过滤*/
	function filter(obj) {
		if (obj.status == 1) {
			obj._color = 'danger';
			obj._msg = '待审核';
		} else if (obj.status == 0) {
			obj._color = 'success';
			obj._msg = '正常';
		} else if (obj.status == 2) {
			obj._color = 'secondary';
			obj._msg = '冻结';
		} else if (obj.status == 10) {
			obj._color = 'default';
			obj._msg = '已注销';
		}
	}

	/**详情*/
	function userDetail(id) {
		U.showHtmlDialog('用户信息', "detail?id="+id, '800', '500');
	}

	/**编辑*/
	function userEdit(url) {
		U.showHtmlDialog('用户编辑', url, '', '800')
	}

	/**删除*/
	function userRemove(id) {
		U.confirm('确认要删除吗？', function() {
			ajaxCall("user/remove.do", {
				"id" : id
			}, function() {
				U.msg('已删除', 500, function() {
					load();
				});
			});
		});
	}

	/**重置密码*/
	function resetPwd(id) {
		U.confirm('确认重置密码为123456？', function() {
			ajaxCall("user/resetPwd.do", {
				"id" : id
			}, function() {
				U.msg('密码已重置', 500, function() {
					load();
				});
			});
		});
	}

	/**修改状态*/
	function changeStatus(obj, status, id) {
		U.confirm('确认将状态置为' + $(obj).attr("title") + '？', function() {
			ajaxCall("user/changeStatus.do", {
				"id" : id,
				"status" : status
			}, function() {
				U.msg('操作成功', 500, function() {
					load();
				});
			});
		});
	}
</script>
</html>