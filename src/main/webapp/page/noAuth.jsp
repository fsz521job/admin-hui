﻿<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- 防止集成Shiro后当遇到404错误时会丢失session -->
<%@ page session="false"%>
<!DOCTYPE HTML>
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/favicon.ico" media="screen" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui.admin/css/H-ui.login.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/static/h-ui.admin/css/H-ui.admin.css" rel="stylesheet" type="text/css" />
<link href="${pageContext.request.contextPath}/static/H-ui.admin_v3.0/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
<title>404页面</title>
</head>
<body>
	<section class="container-fluid page-404 minWP text-c">
		<p class="error-title">
			<i class="Hui-iconfont va-m" style="font-size:80px">&#xe688;</i><span class="va-m"> 没有权限</span>
		</p>
		<p class="error-description">对不起，您没有相应权限~</p>
	</section>
</body>
</html>