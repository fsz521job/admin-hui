package com.chensi.activemq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.chensi.activemq.producer.QueueSender;
import com.chensi.activemq.producer.TopicSender;
import com.chensi.common.AjaxJson;
import com.chensi.common.Constants;

/**
 * 消息队列
 * @author chensi
 * @version 2016-9-3 下午6:47:28
 */
@Controller
@RequestMapping("message")
public class ActivemqController {

	@Autowired
	private QueueSender queueSender;

	@Autowired
	private TopicSender topicSender;

	/**
	 * 消息队列页面
	 * @return
	 */
	@RequestMapping("viewActivemq")
	public ModelAndView viewActivemq() {
		return new ModelAndView("message/activemq");
	}

	/**
	 * 发送消息到队列
	 * @param message
	 * @return String
	 */
	@RequestMapping("queue.do")
	@ResponseBody
	public AjaxJson queue(String text) {
		queueSender.send(Constants.MQ_QUEUE, text);
		return AjaxJson.getSuccessInfo();
	}

	/**
	 * 发送消息到主题
	 * @param message
	 * @return String
	 */
	@RequestMapping("topic.do")
	@ResponseBody
	public AjaxJson topic(String text) {
		topicSender.send(Constants.MQ_TOPIC, text);
		return AjaxJson.getSuccessInfo();
	}
}
