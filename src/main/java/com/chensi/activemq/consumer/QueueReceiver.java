package com.chensi.activemq.consumer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class QueueReceiver implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(QueueReceiver.class);

	@Override
	public void onMessage(Message message) {
		try {
			logger.info("QueueReceiver1接收到消息:{}", ((TextMessage) message).getText());
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
