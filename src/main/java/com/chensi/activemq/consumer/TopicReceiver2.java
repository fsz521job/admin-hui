package com.chensi.activemq.consumer;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class TopicReceiver2 implements MessageListener {

	private static final Logger logger = LoggerFactory.getLogger(TopicReceiver2.class);

	@Override
	public void onMessage(Message message) {
		try {
			logger.info("TopicReceiver2接收到消息:{}", ((TextMessage) message).getText());
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
}
