替换myeclipse的jar，解决无法导出war包问题

Window-Preferences:
1 General--ContentTypes--Java Properties File==>utf-8
2.General--ContentTypes--JSP==>utf-8
3.General--Startup and Shutdown
4.General--workspace
4.General--Ediors--TextEditors--spell==>disable
4.General--Ediors--TextEditors--show line numbers
5.Java--Code Style--Code Templates--Comments-Types  @author Chensi @version ${date} ${time}
6.Java--Code Style--Formatter 自定义格式换行   160px comments:取消New Line after @param tags, 取消Blank line before javadoc tags
7.Java--Compiler==>jdk 1.7
8.Java--Compiler--Errors/Warings--Generic types==>ignore(忽略泛型警告)
9.JAva--Installed JREs==>jdk 1.7==>-Xms512m -Xmx2048m -XX:MaxPermSize=512m
10.MyEclipse--Maven4MyEclipse==>取消Download
11.MyEclipse--Servers--Tomcat 7.x--JDK==>jdk1.7  -Xms512m  -Xmx2048m  -XX:MaxPermSize=512m  debugmode
11.小服务器图标--移除其他服务器server
12.MyEclipse--Validation 关闭验证
13.Team--Ignore Resources==>选择不需要提交的文件后缀 .settings .classpath .project .mymetadata .myeclipse .externalToolBuilders
14.MyEclipse--File and editors--jsp==>utf-8  模板选择jsp with html markedup==>html5
14.MyEclipse--File and editors--javascript--codeStyle--formatter   lineWapper==>160
15.jsp快捷键==》关于在JSP中Ctrl+shift+c注释快捷键不好用的解决方法。这是因为JSP默认打开方式不支持。把打开方式改为MyEclipse JSP Editor 就行了。每次都这样改会很不方便，可以改一下JSP
的默认打开方式。preferences->General->editors－>Files  Associations（位置根据具体eclipse版本会有所不同）  File   Types   选择.jsp。   在  assiciations editors  
里选择MyEclipse JSP Editor ，然后点右边的default。OK了。
(同理设置html==>myeclipse html editor)
16.代码栏长度  1.java==>formatter-line wrapping左上角   2.html,jsp,xml==》file and editor-html-html source 

Window-Customize Per...:移除不需要的组件

项目右击:
1.Resource--Builders==>关闭js校验
2.java build path==>jre system library添加access rules(引用sun包里的类)
3.java compiler==>jdk 1.7
4.MyEclipse--Project Facets==>java -version 1.7
5.引入sun jar包 ：右键项目 -->Properties -->Java Bulid Path-> Libraries -->JRE System Library-->Access rules -->
双击Type Access Rules在Accessible中添加accessible，下面填上** 点击确定。

myeclipse.ini:
-vmargs
-Xms512m              （ JAVA能够分配的内存）
-Xmx2048m              （ JAVA能够分配的最大内存）
-XX:PermSize=256M      （非堆内存初始值）
-XX:MaxPermSize=512M   （非堆内存最大值）


maven:Maven4MyEclipse
1.installation:
select the installation used to launch maven==>/work/programs/apache-maven-3.3.9
Global settings from installation directory==>/work/programs/apache-maven-3.3.9/conf/settings.xml
2.user settings
user settings==>/work/programs/apache-maven-3.3.9/conf/settings.xml
local repository==>/work/programs/apache-maven-3.3.9/repository
3.环境变量设置maven_home==>D:\apache-maven-3.2.1
4.环境变量设置path==>;%maven_home%\bin;

maven:优化
1防止添加本地jar的时候搜索到远程jar
maven repositories--Global Repositories--central==>disable
2本地jar索引库更新（jar发生人为文件变动必须更新它）
maven repositories--Local Repositories--Local Repositories==>rebuild


tomcat:
修改get请求参数utf-8编码
<Connector port="8009" enableLookups="false" redirectPort="8443"
debug="0" protocol="AJP/1.3" URIEncoding="UTF-8"/>

Myeclipse 2013修改字体
　　MyEclipse 2013是基于Eclipse3.7内核，但在Eclipse的Preferences-〉general-〉 Appearance->Colors and Fonts 中并没有找到Courier New字体,它采用的是Consolas字体,中文看着非常小非常别扭,在Windows7下，系统自带虽然有Courier New字体，但是并没有激活显示，需要手动激活，才能在软件中使用。
激活方法如下：
1、在win7的控制面板->字体，找到Courier New，右键，显示。 Courier 常规 ，右键，显示。
2、window-->preferences-->搜索font-->Color and Font-->Basic-->Text Font-->Edit--> Courier New-->常规-->四号 
注：也可以用YaHei.Consolas.1.12字体，见附件。


myeclipse修改项目webroot
项目==》java build path==>default out folder 改为实际的
项目==》myeclipse==>web 修改为实际的

C盘maven库为自己的网络环境库；D盘的maven库为公司私服库；所以配置maven参数的时候注意区分好(注意conf.xml里面配置的repository路径)
