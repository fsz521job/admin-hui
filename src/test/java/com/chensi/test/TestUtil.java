package com.chensi.test;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.junit.Test;

import com.alibaba.fastjson.JSON;



public class TestUtil {

	/**
	 * map转json
	 */
	@Test
	public void test1(){
		Map<String, Object> map=new HashMap<String, Object>();
		map.put("taskId", "4f681df54dc54928810f1537bb1c78ba");
		map.put("projectCode", "DLGXGC201701002");
		JSONObject json=JSONObject.fromObject(map);
		//json-lib
		System.out.println(json.toString());
		//fastjson
		System.out.println(JSON.toJSONString(map));
	}
	
	@Test
	public void test2(){
		JSONArray js=JSONArray.fromObject("[1,2,3]");
		for(Object o:js){
			System.out.println(o);
		}
	}
}
