package com.chensi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.chensi.common.AjaxJson;
import com.chensi.model.ModelDto;
import com.chensi.model.School;
import com.chensi.model.Student;

@Controller
@RequestMapping("testParam")
public class ParamController {

	/**
	 * 请求参数listStr=1,2,3,aa,bb
	 * 返回{"status":"000000","data":{"id":null,"listStr":["1","2","3","aa","bb"]},"message":"成功"}
	 */
	@ResponseBody
	@RequestMapping("listString.json")
	public AjaxJson listString(ModelDto model){
		return AjaxJson.getSuccessInfo(model);
	}
	
	/**
	 * 请求参数listStudent[0].id=1&listStudent[0].name=ss&listStudent[1].id=2&listStudent[1].name=cc
	 * 返回{"status":"000000","data":{"id":null,"listStr":null,"listStudent":[{"id":1,"name":"ss"},{"id":2,"name":"cc"}]},"message":"成功"}
	 */
	@ResponseBody
	@RequestMapping("listObject.json")
	public AjaxJson listObject(ModelDto model){
		return AjaxJson.getSuccessInfo(model);
	}
	
	/**
	 * 请求参数listMap[0][a]=陈&listMap[0][b]=sss&listMap[1][c]=ccc
	 * 返回{"status":"000000","data":{"id":null,"listStr":null,"listStudent":null,"listMap":[{"a":"陈司","b":"bbb"},{"c":"ccc"}]},"message":"成功"}
	 */
	@ResponseBody
	@RequestMapping("listMap.json")
	public AjaxJson listMap(ModelDto model){
		return AjaxJson.getSuccessInfo(model);
	}
	
	/**
	 * @param map[a]=aaa&map[b]=bbb
	 * @return {"status":"000000","data":{"id":null,"listStr":null,"listStudent":null,"listMap":null,"map":{"a":"aaa","b":"bbb"}},"message":"成功"}
	 */
	@ResponseBody
	@RequestMapping("map.json")
	public AjaxJson map(ModelDto model){
		return AjaxJson.getSuccessInfo(model);
	}
	
	/**
	 * @param jsonStr=[{"a":"aaa","b":"bbb"}]
	 * @return {"status":"000000","data":[{"b":"bbb","a":"aaa"}],"message":"成功"}
	 */
	@ResponseBody
	@RequestMapping("jsonArr.json")
	public AjaxJson jsonArr(String jsonStr){
		return AjaxJson.getSuccessInfo(JSONObject.parseArray(jsonStr));
	}
	
	/**
	 * @param jsonStr={"a":null,"b":""}
	 * @return {"status":"000000","data":{"b":"","a":null},"message":"成功"}
	 */
	@ResponseBody
	@RequestMapping("jsonObj.json")
	public AjaxJson jsonObj(String jsonStr){
		return AjaxJson.getSuccessInfo(JSONObject.parse(jsonStr));
	}
	
	/**
	 * @param name=xxx&address=ccc
	 * @return {"status":"000000","data":"Student [id=null, name=xxx],School [id=null, name=xxx, address=ccc]","message":"成功"}
	 * 结论：只要存在相同属性都会赋值
	 */
	@ResponseBody
	@RequestMapping("twoObj.json")
	public AjaxJson twoObj(Student student,School school){
		return AjaxJson.getSuccessInfo(student+","+school);
	}
	
}
