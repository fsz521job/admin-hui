/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : admin-hui

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-09-04 15:02:43
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_menu`;
CREATE TABLE `t_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COMMENT='菜单';

-- ----------------------------
-- Records of t_menu
-- ----------------------------
INSERT INTO `t_menu` VALUES ('1', null, '1', '根目录', 'root', '0', '', '1.png');
INSERT INTO `t_menu` VALUES ('2', '1', '1', '系统管理', 'index:system:all', '1', '', 'computer.png');
INSERT INTO `t_menu` VALUES ('3', '1', '1', '人员管理', 'index:user:all', '2', '', 'group.png');
INSERT INTO `t_menu` VALUES ('4', '3', '1', '用户管理', 'index:user:user', '3', 'user/main', 'user.png');
INSERT INTO `t_menu` VALUES ('5', '3', '1', '角色管理', 'index:user:role', '4', 'role/main', 'transmit.png');
INSERT INTO `t_menu` VALUES ('6', '3', '1', '菜单管理', 'index:user:menu', '5', 'menu/main', 'page.png');
INSERT INTO `t_menu` VALUES ('7', '4', '2', '添加', 'user:add', '6', '', 'user_add.png');
INSERT INTO `t_menu` VALUES ('8', '4', '2', '修改', 'user:update', '7', '', 'user_edit.png');
INSERT INTO `t_menu` VALUES ('9', '4', '2', '删除', 'user:delete', '8', '', 'user_delete.png');
INSERT INTO `t_menu` VALUES ('10', '5', '2', '添加', 'role:add', '9', '', 'transmit_add.png');
INSERT INTO `t_menu` VALUES ('11', '5', '2', '修改', 'role:update', '10', '', 'transmit_edit.png');
INSERT INTO `t_menu` VALUES ('12', '5', '2', '删除', 'role:delete', '11', '', 'transmit_delete.png');
INSERT INTO `t_menu` VALUES ('13', '6', '2', '添加', 'menu:add', '12', '', 'page_add.png');
INSERT INTO `t_menu` VALUES ('14', '6', '2', '修改', 'menu:update', '13', '', 'page_edit.png');
INSERT INTO `t_menu` VALUES ('15', '6', '2', '删除', 'menu:delete', '14', '', 'page_delete.png');
INSERT INTO `t_menu` VALUES ('16', '1', '1', '统计管理', 'index:statistic:all', '15', '', 'chart_bar.png');
INSERT INTO `t_menu` VALUES ('17', '1', '1', '设备管理', 'index:device:all', '16', '', 'bricks.png');
INSERT INTO `t_menu` VALUES ('18', '1', '1', '图片管理', 'index:pic:all', '17', '', 'photo.png');
INSERT INTO `t_menu` VALUES ('20', '17', '1', '设备管理', 'index:device:device', '18', '', 'cog.png');
INSERT INTO `t_menu` VALUES ('21', '17', '1', '故障管理', 'index:device:problem', '19', '', 'bug.png');
INSERT INTO `t_menu` VALUES ('22', '20', '2', '添加', 'device:add', '20', '', 'cog_add.png');
INSERT INTO `t_menu` VALUES ('23', '4', '2', '详情', 'user:detail', '21', '', 'user_go.png');
INSERT INTO `t_menu` VALUES ('24', '4', '2', '冻结', 'user:toFrozen', '22', '', 'group_link.png');
INSERT INTO `t_menu` VALUES ('25', '4', '2', '正常', 'user:toNormal', '23', '', 'user_green.png');
INSERT INTO `t_menu` VALUES ('26', '4', '2', '批量删除', 'user:deleteAll', '24', '', 'user_delete.png');
INSERT INTO `t_menu` VALUES ('27', '4', '2', '密码重置', 'user:resetPwd', '25', '', 'key_go.png');
INSERT INTO `t_menu` VALUES ('28', '5', '2', '授权', 'role:grant', '26', '', 'transmit_go.png');
INSERT INTO `t_menu` VALUES ('29', '1', '1', '测试管理', 'demo', '31', '', 'table_gear.png');
INSERT INTO `t_menu` VALUES ('32', '1', '1', '消息管理', 'index:message:all', '30', '', 'comments.png');
INSERT INTO `t_menu` VALUES ('33', '32', '1', '即时聊天', 'message:im', '32', '', 'user_comment.png');
INSERT INTO `t_menu` VALUES ('34', '32', '1', '广播', 'message:broadcast', '33', 'message/viewBroadcast', 'cd.png');
INSERT INTO `t_menu` VALUES ('35', '32', '1', '消息队列', 'message:activemq', '34', 'message/viewActivemq', 'building.png');
INSERT INTO `t_menu` VALUES ('36', '1', '1', '远程服务', 'index:rpc:all', '35', '', 'computer_link.png');
INSERT INTO `t_menu` VALUES ('37', '36', '1', 'dubbo接口', 'rpc:dubbo', '36', 'rpc/viewDubbo', 'computer_go.png');
INSERT INTO `t_menu` VALUES ('38', '16', '1', 'echarts图表', 'statistic:echarts', '37', 'static/echarts.jsp', 'chart_curve.png');
INSERT INTO `t_menu` VALUES ('39', '16', '1', 'highcharts图表', 'statistic:highcharts', '38', 'static/highcharts.jsp', 'chart_line.png');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', '系统管理员', '1', '拥有所有权限');
INSERT INTO `t_role` VALUES ('2', '检查员', '6', '只能进行检查任务');
INSERT INTO `t_role` VALUES ('3', '省级质检管理员', '3', '省级质检，负责省级以下质检');
INSERT INTO `t_role` VALUES ('4', '市级质检管理员', '4', '市级质检，负责市级以下质检');
INSERT INTO `t_role` VALUES ('5', '区县级质检管理员', '5', '负责区县级质检');
INSERT INTO `t_role` VALUES ('6', '1', '7', '');
INSERT INTO `t_role` VALUES ('7', '2', '8', '');
INSERT INTO `t_role` VALUES ('8', '3', '9', '');
INSERT INTO `t_role` VALUES ('9', '4', '10', '');
INSERT INTO `t_role` VALUES ('10', '5', '11', '');
INSERT INTO `t_role` VALUES ('11', '6', '12', '');
INSERT INTO `t_role` VALUES ('12', '12121212', '13', '');
INSERT INTO `t_role` VALUES ('13', 'aS', '14', '');

-- ----------------------------
-- Table structure for t_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `t_role_menu`;
CREATE TABLE `t_role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roleId` int(11) DEFAULT NULL,
  `menuId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=681 DEFAULT CHARSET=utf8 COMMENT='角色菜单';

-- ----------------------------
-- Records of t_role_menu
-- ----------------------------
INSERT INTO `t_role_menu` VALUES ('34', '5', '1');
INSERT INTO `t_role_menu` VALUES ('35', '5', '16');
INSERT INTO `t_role_menu` VALUES ('36', '5', '17');
INSERT INTO `t_role_menu` VALUES ('39', '3', '1');
INSERT INTO `t_role_menu` VALUES ('40', '3', '3');
INSERT INTO `t_role_menu` VALUES ('41', '3', '4');
INSERT INTO `t_role_menu` VALUES ('42', '3', '7');
INSERT INTO `t_role_menu` VALUES ('43', '3', '8');
INSERT INTO `t_role_menu` VALUES ('44', '3', '9');
INSERT INTO `t_role_menu` VALUES ('45', '3', '16');
INSERT INTO `t_role_menu` VALUES ('46', '3', '17');
INSERT INTO `t_role_menu` VALUES ('47', '3', '18');
INSERT INTO `t_role_menu` VALUES ('130', '4', '1');
INSERT INTO `t_role_menu` VALUES ('131', '4', '17');
INSERT INTO `t_role_menu` VALUES ('132', '4', '18');
INSERT INTO `t_role_menu` VALUES ('379', '8', '1');
INSERT INTO `t_role_menu` VALUES ('380', '8', '3');
INSERT INTO `t_role_menu` VALUES ('381', '8', '4');
INSERT INTO `t_role_menu` VALUES ('382', '8', '7');
INSERT INTO `t_role_menu` VALUES ('439', '2', '1');
INSERT INTO `t_role_menu` VALUES ('440', '2', '3');
INSERT INTO `t_role_menu` VALUES ('441', '2', '4');
INSERT INTO `t_role_menu` VALUES ('442', '2', '8');
INSERT INTO `t_role_menu` VALUES ('443', '2', '23');
INSERT INTO `t_role_menu` VALUES ('444', '2', '5');
INSERT INTO `t_role_menu` VALUES ('445', '2', '11');
INSERT INTO `t_role_menu` VALUES ('446', '2', '6');
INSERT INTO `t_role_menu` VALUES ('447', '2', '14');
INSERT INTO `t_role_menu` VALUES ('448', '2', '17');
INSERT INTO `t_role_menu` VALUES ('449', '2', '21');
INSERT INTO `t_role_menu` VALUES ('450', '2', '29');
INSERT INTO `t_role_menu` VALUES ('645', '1', '1');
INSERT INTO `t_role_menu` VALUES ('646', '1', '2');
INSERT INTO `t_role_menu` VALUES ('647', '1', '3');
INSERT INTO `t_role_menu` VALUES ('648', '1', '4');
INSERT INTO `t_role_menu` VALUES ('649', '1', '7');
INSERT INTO `t_role_menu` VALUES ('650', '1', '8');
INSERT INTO `t_role_menu` VALUES ('651', '1', '9');
INSERT INTO `t_role_menu` VALUES ('652', '1', '23');
INSERT INTO `t_role_menu` VALUES ('653', '1', '24');
INSERT INTO `t_role_menu` VALUES ('654', '1', '25');
INSERT INTO `t_role_menu` VALUES ('655', '1', '26');
INSERT INTO `t_role_menu` VALUES ('656', '1', '27');
INSERT INTO `t_role_menu` VALUES ('657', '1', '5');
INSERT INTO `t_role_menu` VALUES ('658', '1', '10');
INSERT INTO `t_role_menu` VALUES ('659', '1', '11');
INSERT INTO `t_role_menu` VALUES ('660', '1', '12');
INSERT INTO `t_role_menu` VALUES ('661', '1', '28');
INSERT INTO `t_role_menu` VALUES ('662', '1', '6');
INSERT INTO `t_role_menu` VALUES ('663', '1', '13');
INSERT INTO `t_role_menu` VALUES ('664', '1', '14');
INSERT INTO `t_role_menu` VALUES ('665', '1', '15');
INSERT INTO `t_role_menu` VALUES ('666', '1', '16');
INSERT INTO `t_role_menu` VALUES ('667', '1', '38');
INSERT INTO `t_role_menu` VALUES ('668', '1', '39');
INSERT INTO `t_role_menu` VALUES ('669', '1', '17');
INSERT INTO `t_role_menu` VALUES ('670', '1', '20');
INSERT INTO `t_role_menu` VALUES ('671', '1', '22');
INSERT INTO `t_role_menu` VALUES ('672', '1', '21');
INSERT INTO `t_role_menu` VALUES ('673', '1', '18');
INSERT INTO `t_role_menu` VALUES ('674', '1', '29');
INSERT INTO `t_role_menu` VALUES ('675', '1', '32');
INSERT INTO `t_role_menu` VALUES ('676', '1', '33');
INSERT INTO `t_role_menu` VALUES ('677', '1', '34');
INSERT INTO `t_role_menu` VALUES ('678', '1', '35');
INSERT INTO `t_role_menu` VALUES ('679', '1', '36');
INSERT INTO `t_role_menu` VALUES ('680', '1', '37');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `tel` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `openid` varchar(50) DEFAULT NULL,
  `province` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `area` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `idcard` varchar(50) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `sign` varchar(50) DEFAULT NULL,
  `attach` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `isSuper` int(11) DEFAULT NULL,
  `createTime` varchar(50) DEFAULT NULL,
  `lastTime` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'admin', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '陈司', '15365193930', '479964140@qq.com', 'xxxx', '江苏', '南京', '六合区', '南湖大道888号', '32091111111', 'images/head.png', '天天放假~', 'uploadFiles/2016/08/08/147067178235784595.jpg', '我是admin！！', '0', '1', '2016-08-06 22:53:53', '2016-09-04 14:37:29');
INSERT INTO `t_user` VALUES ('2', 'user2', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', '风一样的男子', 'uploadFiles/2016/08/09/147067248976444123.jpg', '你猜', '0', '0', '2016-08-06 22:53:53', '2016-08-26 19:04:33');
INSERT INTO `t_user` VALUES ('3', 'user3', 'E10ADC3949BA59ABBE56E057F20F883E', '3', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '江苏', '南京', '玄武区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('4', 'user4', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '', '', '桥西路888号', '32091111111', 'images/head.png', null, 'uploadFiles/2016/08/07/147057177491853261.jpg', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('5', 'user5', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', '', '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('6', 'user6', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('7', 'user7', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('8', 'user8', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('9', 'user9', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('11', 'user11', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('12', 'user12', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('13', 'user13', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('14', 'user14', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('15', 'user15', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('16', 'user16', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('18', 'user18', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('19', 'user19', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('20', 'user20', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('21', 'user21', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('22', 'user22', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('23', 'user23', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '1', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('24', 'user24', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('25', 'user25', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('26', 'user26', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('27', 'user27', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '0', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('28', 'user28', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '天津', '天津', '和平区', '西湖路888号', '32091111111345', 'images/head.png', null, 'C:\\fakepath\\09.jpg', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('29', 'user29', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('30', 'user30', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '', '', '南京路222', '32091111111', 'images/head.png', null, 'uploadFiles/2016/08/07/147057179125863608.jpg', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('31', 'user31', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('32', 'user32', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('33', 'user33', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('34', 'user34', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('35', 'user35', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('36', 'user36', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('37', 'user37', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('38', 'user38', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('39', 'user39', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('40', 'user40', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('41', 'user41', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('42', 'user42', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('43', 'user43', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('44', 'user44', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('45', 'user45', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('46', 'user46', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('47', 'user47', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('48', 'user48', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('50', 'user50', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('52', 'user52', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('53', 'user53', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('54', 'user54', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('57', 'user57', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('58', 'user58', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('60', 'user60', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('61', 'user61', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('62', 'user62', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('63', 'user63', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('64', 'user64', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('65', 'user65', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('66', 'user66', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('67', 'user67', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('68', 'user68', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('69', 'user69', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('70', 'user70', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('71', 'user71', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('72', 'user72', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('73', 'user73', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('74', 'user74', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('75', 'user75', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('76', 'user76', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('77', 'user77', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('78', 'user78', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('79', 'user79', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('80', 'user80', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('81', 'user81', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('82', 'user82', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('83', 'user83', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('84', 'user84', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('85', 'user85', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '0', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('86', 'user86', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('87', 'user87', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('88', 'user88', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('89', 'user89', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('90', 'user90', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('91', 'user91', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('92', 'user92', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('93', 'user93', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('94', 'user94', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('95', 'user95', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('96', 'user96', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('97', 'user97', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('98', 'user98', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('99', 'user99', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('100', 'user100', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('101', 'user101', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('102', 'user102', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('103', 'user103', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('104', 'user104', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('105', 'user105', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('106', 'user106', 'E10ADC3949BA59ABBE56E057F20F883E', '4', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '天津', '天津', '和平区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '0', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('107', 'user107', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('108', 'user108', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('109', 'user109', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('110', 'user110', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('111', 'user111', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('112', 'user112', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('113', 'user113', 'E10ADC3949BA59ABBE56E057F20F883E', '3', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', '', '', '', '0', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('114', 'user114', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('115', 'user115', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('116', 'user116', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('117', 'user117', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '0', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('118', 'user118', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('119', 'user119', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('120', 'user120', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '李青', '15365193111', '1212@qq.com', 'xxxx2345245', '上海', '上海', '徐汇区', '南京路222', '32091111111', 'images/head.png', null, '', '你猜', '1', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('121', 'user121', 'E10ADC3949BA59ABBE56E057F20F883E', '2', '张云召', '15365122222', 'sdfdsfg40@qq.com', 'xxxx1212', '浙江', '杭州', '西湖区', '西湖路888号', '32091111111345', 'images/head.png', null, '', '', '2', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('122', 'user122', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '马丽莎', '15365193930', '479964140@qq.com', 'xxxx', '河北', '邢台', '桥西区', '桥西路888号', '32091111111', 'images/head.png', null, '', '', '10', '0', '2016-08-06 22:53:53', '2016-08-06 22:53:53');
INSERT INTO `t_user` VALUES ('123', 'chentt', 'E10ADC3949BA59ABBE56E057F20F883E', '5', '陈天浩', '13266666666', '479964140@qq.com', 'xxxx2345245', '河北', '石家庄', '长安区', 'xxxx', 'xxxx', 'images/head.png', null, 'uploadFiles/2016/08/09/147067242708673596.jpg', '', '0', '0', '2016-08-07 18:31:13', '2016-08-07 18:31:13');
INSERT INTO `t_user` VALUES ('124', 'zth', 'E10ADC3949BA59ABBE56E057F20F883E', '3', '赵天虎', '15365122222', '12@qwq.ed', 'xxxx1212', '天津', '天津', '和平区', '南京路222', '32091111111345', 'images/head.png', null, 'uploadFiles/2016/08/08/147067177213260446.jpg', '', '0', '0', '2016-08-07 20:09:23', '2016-08-07 20:09:23');
INSERT INTO `t_user` VALUES ('125', 'user', 'E10ADC3949BA59ABBE56E057F20F883E', '1', '丁笑笑', '15365193930', '479964140@qq.com', '', '天津', '天津', '和平区', '', '', 'images/head.png', '无敌', 'uploadFiles/2016/08/23/14719665081899291.jpg', '', '0', '0', '2016-08-23 23:34:34', '2016-09-02 21:50:59');
